({
    appDir: "src",
    baseUrl: "js",
    dir: "build",
    mainConfigFile: 'src/js/config.build.js',
    modules: [
        {
            name: "main"
        }
    ]
})
