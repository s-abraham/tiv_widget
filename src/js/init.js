/*
 * @author Freddy Knuth
 * 
 * Initialize the widget by setting up all the variables needed 
 * to kick start the app. (mainly the dealerkey)
 * 
 * Also all the URLs for the services will be found here as an
 * easy way to change them in case the endpoint for them changes
 * 
 * 
 */
require.config({
    deps: ['text', 'jquery', 'underscore', 'backbone', 'bootstrap', 'underscore.string'],
    paths: {
        //Base Libraries
        text: 'vendor/text',
        jquery: 'vendor/jquery',
        underscore: 'vendor/underscore',
        backbone: 'vendor/backbone',
        bootstrap: 'vendor/bootstrap',
        'backbone.modelbinder': 'vendor/Backbone.ModelBinder',
        'underscore.string': 'vendor/underscore.string',

        //Jquery Plugins
        'jquery-mobile-detect': 'vendor/jquery-mobile-detect',
        'jquery-ui-datepicker': 'vendor/jquery-ui-datepicker',
        'jquery.ui.widget': 'vendor/jquery.fileupload/jquery.ui.widget',
        'jquery.selectbox': 'vendor/jquery.selectBox',
        'jquery.uniform': 'vendor/jquery.uniform',
        'jquery.flash': 'vendor/jquery.flash',
        'jquery.colorbox': 'vendor/jquery.colorbox',
        'jquery.jqprint': 'vendor/jquery.jqprint',
        'jquery.cookie': 'vendor/jquery.cookie',

        //Fileupload Plugins
        'jquery.fileupload': 'vendor/jquery.fileupload/jquery.fileupload',
        'tmpl': 'vendor/jquery.fileupload/tmpl',
        'load-image': 'vendor/jquery.fileupload/load-image',
        'canvas-to-blob': 'vendor/jquery.fileupload/canvas-to-blob',
        'jquery.fileupload-fp': 'vendor/jquery.fileupload/jquery.fileupload-fp',
        'jquery.fileupload-ui': 'vendor/jquery.fileupload/jquery.fileupload-ui',

        //Google Maps
        'gmap3': 'vendor/gmap3'

    },
    shim: {
        'underscore': {
            exports: '_'
        },
        'underscore.string': {
            deps: ['underscore'],
            exports: '_s'
        },
        'backbone': {
            deps: ['jquery', 'underscore', 'underscore.string'],
            exports: 'Backbone'
        },
        'backbone.modelbinder': {
            deps: ['backbone']
        },
        'jquery': {
            exports: 'jQuery'
        },
        'jquery-ui-datepicker': ['jquery'],
        'jquery.fileupload': ['jquery',
                               'jquery.ui.widget',
                               'vendor/jquery.fileupload/jquery.iframe-transport'],

        'jquery.fileupload-ui': ['jquery.fileupload'],
        'jquery.fileupload-fp': ['jquery.fileupload'],

        'jquery.flash': ['jquery'],
        'gmap3': ['jquery'],
        'jquery.cookie': ['jquery'],
        'jquery.selectbox': ['jquery'],
        'jquery.uniform': ['jquery'],
        'bootstrap': ['jquery']
    }
})


define(['require',
        'underscore',
        'backbone',
        'text!tpl/container.html',
        'text!tpl/step1.html'],
        function (require, _, Model, Container, Step1) {

            //Get parameters from the query string in the URL
            var QueryParams = _.bind(function (param) {
                //Parse the URL query to get the dealerkey
                if (_.isEmpty(this.params)) {
                    var params = window.location.search.replace('?', '');
                    var pairs = params.split('&');
                    _.each(pairs, function (v, i) {
                        var pair = v.split('=');
                        if (!_.isUndefined(pair[0]) && !_.isUndefined(pair[1]) && pair[0] != "") {
                            this.params[pair[0]] = pair[1];
                        }
                    }, this);
                }
                return this.params[param]
            }, { params: {} });

            //Define all the URLs for each of the requests, 
            //these can be overriden here if needed
            var URL = _.bind(function (type) {
                var url = typeof this[type] !== 'undefined' ? this[type] : '';
                if (!(new RegExp('^(http(s)?[:]//)', 'i')).test(url) && url != '') {
                    return $(document.body).data('base') + url;
                }
                return url;
            }, {
                dealership: '/dealer/',
                tradein: '/tradein/',
                upload: '/upload/',
                desired: '/desiredvehicle/',
                inventory: '/desiredvehicle/',
                promotion: '/specialoffer/',
                promotion_image: '/specialoffer/?getimage=1',
                appointment: '/appointment/',
                appraisal: '/lead/'
            });

            //Grab dealerkey from query variables
            var dealerkey = QueryParams("dealerkey");

            //Grab the extended parameters
            var e_params = {};
            e_params.vehicleid = decodeURIComponent(QueryParams("vehicleid"));
            e_params.year = decodeURIComponent(QueryParams("year"));
            e_params.make = decodeURIComponent(QueryParams("make"));
            e_params.model = decodeURIComponent(QueryParams("model"));
            e_params.stocknumber = decodeURIComponent(QueryParams("stocknumber"));
            e_params.price = decodeURIComponent(QueryParams("price"));
            e_params.mileage = decodeURIComponent(QueryParams("mileage"));
            e_params.imageurl = decodeURIComponent((QueryParams("imageurl")));
            e_params.internaldealerid = decodeURIComponent(QueryParams("internaldealerid"));
            e_params.dname = decodeURIComponent(QueryParams("dealername"));
            e_params.daddress = decodeURIComponent(QueryParams("dealeraddress"));
            e_params.dcity = decodeURIComponent(QueryParams("dealercity"));
            e_params.dstate = decodeURIComponent(QueryParams("dealerstate"));
            e_params.dzip = decodeURIComponent(QueryParams("dealerzip"));
            e_params.dphone = decodeURIComponent(QueryParams("dealerphone"));
            
            e_params.qmode = false;
                        
            if (
                e_params.year != null && e_params.year != '' && e_params.year != 'undefined'
                && e_params.make != null && e_params.make != '' && e_params.make != 'undefined'
                && e_params.model != null && e_params.model != '' && e_params.model != 'undefined'
                && e_params.imageurl != null && e_params.imageurl != '' && e_params.imageurl != 'undefined'
                && e_params.internaldealerid != null && e_params.internaldealerid != '' && e_params.internaldealerid != 'undefined'
            ) e_params.qmode = true;

            
            //Set the URL for the dealership to the one provided by the URL function
            var dealership = Backbone.Model.extend({
                //url: '/tivwidget/js/dealership.json', 
                url: URL('dealership'),
                widget_scope: {
                    //Url Getter 
                    URL: URL,

                    //Dealerkey
                    dealerkey: dealerkey,
                    e_params: e_params,

                    //Templates
                    step1: Step1,
                    container: Container
                },
                initialized: false,
                //Initialize the dealership model 
                initialize: function () {
                    this.on('change', this.success_init, this)
                },
                //Fetch dealer information
                fetch: function (dealerkey) {
                    var options = {
                        dataType: 'json',
                        type: "GET",
                        data: {
                            dealerkey: dealerkey //,e_params: e_params
                        },
                        error: this.handle_error
                    };
                    return Backbone.Model.prototype.fetch.call(this, options);
                },
                //Initialize the widget.
                success_init: function (m, r, o) {
                    if (!this.initialized) {
                        require(['jquery', 'models', 'collections', 'main', 'jquery-mobile-detect'], _.bind(function (jQuery, Model, Collection, Widget) {
                            //Instantiate the widget options provided by dealership information    
                            Model.widget_options = new Backbone.Model();
                            Model.dealership = this;

                            //Hide the container at first in the case the NADA logo needs to be hidden 
                            this.widget_scope.container = jQuery(this.widget_scope.container);
                            jQuery(document.body).append(this.widget_scope.container);

                            //Create an instance of the widget so that we can start
                            var widget = new Widget({
                                el: this.widget_scope.container,
                                URL: this.widget_scope.URL,
                                dealerkey: this.widget_scope.dealerkey,
                                e_params: this.widget_scope.e_params,
                                model: Model,
                                collection: Collection
                            });

                            //Set the widget options after instantiating the widget
                            //This will let the widget set it's listeners it needs to 
                            Model.widget_options.set(this.get('Options'));

                            //disable qmode if this is not a reseller
                            if (Model.widget_options.get('IsReseller') === false) {
                                widget.e_params.qmode = false;
                            }

                            //Show the container
                            if (Model.widget_options.get('EnableWidget') === true) {

                                this.widget_scope.container.show();

                                //Initialize the steps
                                widget.init_steps();

                                window.WidgetInstance = widget;
                                this.initialized = true;
                            }
                            else {
                                jQuery(".container").empty();
                            }
                        }, this));
                    }
                },
                //Handle request errors
                handle_error: function (m, xhr, o) {
                    var errorMessage = ["<h1>An Error Occurred loading the Application</h1>"];
                    try {
                        var errorObj = JSON.parse(xhr.responseText);
                        errorMessage.push("<strong>Error from the Server:</strong>")
                        errorMessage.push('<strong style="color: #CF3311">' + errorObj.Message + '</strong>');
                    } catch (e) { }
                    document.body.innerHTML = errorMessage.join('<br />');
                }

            });

            //Initialize the dealership model && Fetch dealership information for dealerkey
            (new dealership()).fetch(dealerkey);
        });