define(function() {
    var lang = {}
    
    
    //English Language Text:
    lang['ENG'] = {
        select_lang: "Select Language",
        title: "Your Free Internet Trade-In Appraisal",
        footer: {
            copy: "This appraisal or this trade-in offer is provided by <span id='dealership-name'>this dealership</span> which is solely responsible for it.<br/>We respect your right to privacy. All information provided will remain confidential and will not be disclosed to any other party. Patent Pending. &copy; 2011, TradeInVelocity. All rights reserved. Copyright NADASC/NADAguides, <span id='current-year'></span> , all rights reserved."
        },
        buttons: {
            reset: "Reset",
            start_over: 'Start Over',
            next: 'Next',
            upload_image: 'Upload Image',
            upload_video: 'Upload Video',
            credit_app: 'Credit Application',
            used_inv: 'Used Inventory',
            new_inv: 'New Inventory'
        },
        selects: {
            select_year: 'Select Year',
            select_make: 'Select Make',
            select_model: 'Select Model',
            select_trim: 'Select Trim'
        },
        tradein: {
            title: "Tell Us About Your Vehicle",
            text: "A simple three step process that allows you to find out what your trade-in's really worth.",
            
            year: 'Year',
            make: 'Make',
            model: 'Model',
            trim: 'Trim',
            
            mileage: 'Mileage',
            zip: 'Zip',
            
            condition: 'Condition',
            condition_fair: 'Fair',
            condition_good: 'Good',
            condition_excellent: 'Excellent',
            condition_text: {
                fair: 'Fair Trade-In values reflect a vehicle in a fair condition. This means a vehicle with significant mechanical defects requiring repairs in order to restore reasonable running condition. Paint, body, and wheel surfaces have considerable damage to their finish, which may include dull or faded (oxidized) paint, small to medium size dents, frame damage, rust of obvious signs of previous repairs. Interior reflects above average wear with inoperable equipment, damaged or missing trim and heavily soiled/permanent imperfections on the headliner, carpet and upholstery. Vehicle may have a branded title and un-true mileage. Vehicle will need substantial reconditioning and repair to be made ready for resale. Some existing issues may be difficult to restore.',
                good: 'Good Trade-In values reflect a vehicle in a Good condition. This means a vehicle that is mechanically sound, but may require some repairs/servicing to pass all necessary inspections. Paint, body, and wheel surfaces have moderate imperfections and a Good finish and shine which can be improved with restorative repair. Interior reflects some soiling and wear in relation to vehicle age with all equipment operable or requiring minimal effort to make operable. Vehicle has a clean title history. Vehicle will need a fair degree of reconditioning to be made ready for resale.',
                excellent: 'Excellent Trade-In values reflect a vehicle in an Excellent condition. This means a vehicle with no mechanical defects and passes all necessary inspections with ease. Paint, body, and wheels have minor surface scratching with a high gloss finish and shine. Interior reflects minimal soiling and wear with all equipment in complete working order. Vehicle has a clean title history. Vehicle will need minimal reconditioning to be made for resale.'
            },
            equipment_title: 'Optional Equipment',
            equipment_text: 'Please select the options that are included on your vehicle.',
            equipment_total: 'Total Additions/Deductions'
        },
        customer: {
            additional: {
                title: 'Additional Information',
                text: 'Your trade appraisal is ready! Please provide us with your contact information so we can send you an email confirmation of our Dealer Promotion. A detailed appraisal report of your vehicle will be displayed on the following page.'
            },
            contact: {
                title: 'Your Contact Details',
                fname: 'First Name',
                lname: 'Last Name',
                email: 'Email Address',
                phone: 'Phone',
                zip: 'Zip'
            },
            desired: {
                // WE Removing Optional
                // title: 'Desired Vehicle Information <span>(optional)</span>',
                title: 'Desired Vehicle Information',
                condition: 'Condition',
                condition_options: {
                    newcar: 'New',
                    usedcar: 'Used',
                    unsure: 'Unsure'
                },
                category: 'Category',
                all: 'All',
                make: 'Make',
                model: 'Model'
            },
            promotion: {
                title: 'Dealer Promotion'
            },
            inventory: {
                title: 'Match Found - <span class="match-count"></span> Vehicles',
                text: 'Select a vehicle to recieve a current and desired replacement vehicle appraisal.',
                price: 'Price',
                select: 'Select this Vehicle'
            }
        },
        review: {
            summary: {
                title: 'Summary',
                text: 'Please print this summary and bring it with you when you visit us. A copy has been sent to your e-mail address and to our sales department.'
            },
            tradein: {
                title: 'Trade-In Vehicle Appraisal',
                vehicle: 'Vehicle:',
                mileage: 'Mileage:',
                zip: 'Zip:',
                range: 'Base Range (<span name="condition2"></span>):',
                opt_equip: 'Optional Equipment:',
                mileage_adj: 'Mileage Adjustment:',
                total: 'Total'
            },
            desired: {
                title: 'Desired Vehicle Replacement',
                vehicle: 'Vehicle:',
                mileage: 'Mileage:',
                zip: 'Zip:',
                tradein_title: 'Trade-In Vehicle',
                desired_title: 'Desired Vehicle',
                compare: {
                    year: 'Year',
                    make: 'Make',
                    model: 'Model',
                    body: 'Body'      
                },
                total: 'Total',
                calculate_pmt: 'Calculate Payment'
            },
            contact: {
                title: 'Your Dealership Contact',
                dealership_info: 'Dealership Information',
                appointment: 'Schedule an Appointment',
                directions: 'Get Directions',
                anytime: "Anytime'"
            },
            directions: {
                title: 'Directions to Dealership'
            },
            rules: {
                statement: {
                    title: 'Vehicle Condition Statement:',
                    text: 'The price quoted above reflects a vehicle in the selected condition. Cost of necessary repairs and reconditioning (if needed) could affect the final appraised value. Depending on the vehicle’s condition and equipment, your dealer might offer you more or less for your vehicle.'
                },
                information: {
                    title: 'Important Information:',
                    text: 'Please ask your dealer about possible savings by taking advantage of state sales tax credits. A sales representative will contact you for further information. This estimate is based on the information provided and is not an offer to purchase your vehicle. The actual offer you receive on your trade-in may be more depending on your vehicle\'s condition.'
                },
                privacy: {
                    title: 'Privacy:',
                    text: 'We respect your right to privacy. All information provided will remain confidential and will not be disclosed to any other party.'
                },
                responsibility: {
                    title: 'Responsibility:',
                    text: 'This appraisal is provided by TradeInVelocity which is solely responsible for it.'
                },
                expiration: {
                    title: 'Expiration:',
                    text: 'Please note this Vehicle Appraisal is valid for <span class="expiration-days"></span> days after the date of the appraisal',
                    text_desc: 'Expires on'
                }, 
            }
        },
        calc: {
            title: "Payment Calculator",
            text: "Complete the fields below to estimate your monthly payment.",
            label: {
                price: 'Vehicle Price:',
                apr: 'Interest Rate (APR):',
                length: 'Length of Loan:',
                down_pmt: 'Down Payment:',
                trade_in: 'Trade-In Value:',
                total: 'Your Monthly Payment:'
            },
            disclaimer: 'This tool is for estimation only and does not represent an offer that can be accepted by you. Please contact your Dealership for finance options.'
            
        },
        car: {
            label: {
                price: 'Price:',
                city_mpg: 'City MPG:',
                hwy_mpg: 'Highway MPG:',
                exterior_color: 'Exterior Color:',
                interior_color: 'Interior Color:',
                stock_number: 'Stock Number:'
            }
        },
        upload: {
            image: {
                text: 'You can upload up to 10 images of the vehicle you currently own. A high quality image is not compulsory, but will help with a more accurate appraisal!',
                label: 'Select File:',
                button: 'Add Files...',
                disclaimer: 'File Types: JPG, JPEG, PNG, BMP, GIF<br />Maximum 10 Images<br />Size Limit: 2MB'
            },
            video: {
                text: 'Upload a video of your Trade in directly to YouTube!',
                label: 'Select File:',
                button: 'Add File...',
                disclaimer: '<p>Please note that it is your responsibility to ensure that all video uploaded to YouTube adheres to YouTube terms and conditions regarding video content, format, size and length.</p>'+
                            '<p>The size of your video will impact the amount of time it takes for the video to upload.</p>',
                youtube_text: 'Already have an uploaded video?',
                youtube_help: 'Enter your YouTube URL in the box above.'
            }
        },
        validation: {
            missing: 'Value is missing',
            phone_empty: 'You must enter in a phone number',
            phone_format: 'Phone number must be complete.  ###-###-####',
            zip: 'Must be a valid zip'
        }
    }
    
    
    //Spanish Language Text
    
    lang['ESP'] = {
        select_lang: "Seleccione Lenguaje",
        // Removed Guarantee Language - WE 02-15-2013
        title: "Internet gratis Tasaci&oacute;n de permuta",
        footer: {
            copy: "=============== XXXXX     THIS NEEDS TO BE TRANSLATED     XXXXXX ====================="
        },
        buttons: {
            reset: "Reiniciar",
            start_over: 'Empezar de nuevo',
            next: 'Siguiente',
            upload_image: 'Subir foto',
            upload_video: 'Subir video',
            credit_app: 'Credit Application',
            used_inv: 'Used Inventory',
            new_inv: 'New Inventory'
        },
        selects: {
            select_year: 'Seleccionar a&ntilde;o',
            select_make: 'Seleccionar marca',
            select_model: 'Seleccionar el modelo',
            select_trim: 'Seleccionar el asiento'
        },
        tradein: {
            title: "H&aacute;blenos de su veh&iacute;culo",
            text: "Ent&eacute;rese del valor del carro que va a ofrecer. Un simple proceso de tres pasos solamente tomar&aacute; un minuto. Esta valoraci&oacute;n tomar&aacute; en cuenta el a&ntilde;o del veh&iacute;culo, millaje, regi&oacute;n y equipamiento opcional.",
            
            year: 'A&ntilde;o',
            make: 'Marca',
            model: 'Modelo',
            trim: 'Estilo',
            
            mileage: 'Millaje',
            zip: 'C&oacute;digo Postal',
            
            condition: 'Condic&iacute;on',
            condition_fair: 'Satisfactorio',
            condition_good: 'Bueno',
            condition_excellent: 'Excelente',
            condition_text: {
                fair: 'Los valores de Trueque de venta &aacute;speros reflejan un veh&iacute;culo en la condici&oacute;n &aacute;spera. Esto significa un veh&iacute;culo con defectos mec&aacute;nicos significativos que requieren reparaciones a fin de restaurar la condici&oacute;n de marcha razonable. La pintura, el cuerpo y las superficies de rueda tienen el da&ntilde;o considerable a su fin, que puede incluir la pintura (oxidada) embotada o descolorada, peque&ntilde;a a abolladuras de tama&ntilde;o medio, da&ntilde;o de marco, herrumbre o se&ntilde;ales obvias de reparaciones anteriores. El interior reflexiona encima de la ropa media con el equipo inoperable, da&ntilde;ado o perdiendo imperfecciones permanentes / netas y pesadamente manchadas en el headliner, alfombra y tapicer&iacute;a. El veh&iacute;culo puede tener un t&iacute;tulo marcado y kilometraje falso. El veh&iacute;culo necesitar&aacute; el reacondicionamiento sustancial y se dirigir&aacute; para ser preparado a la reventa. Algunas cuestiones existentes pueden ser dif&iacute;ciles de restaurar. Como la condici&oacute;n de veh&iacute;culo individual var&iacute;a enormemente, los usuarios de NADAguides.com tendr&iacute;an que hacer ajustes independientes a la condici&oacute;n de veh&iacute;culo actual.',
                good: 'Los valores de Trueque de venta medios reflejan un veh&iacute;culo en la condici&oacute;n media. Esto significa un veh&iacute;culo que es mec&aacute;nicamente sano, pero puede requerir que algunas reparaciones/revisi&oacute;n pasen todas las inspecciones necesarias. La pintura, el cuerpo y las superficies de rueda tienen imperfecciones moderadas y un fin medio y brillo que puede ser mejorado con la reparaci&oacute;n reconstituyente. El interior refleja a unos manchar y ropa con relaci&oacute;n a la edad de veh&iacute;culo con todo el equipo esfuerzo m&iacute;nimo operable o que requiere de hacer operable. El veh&iacute;culo tiene una historia de t&iacute;tulo limpia. El veh&iacute;culo necesitar&aacute; un nivel justo del reacondicionamiento para ser preparado a la reventa. Como la condici&oacute;n de veh&iacute;culo individual var&iacute;a enormemente, los usuarios de NADAguides.com tendr&iacute;an que hacer ajustes independientes a la condici&oacute;n de veh&iacute;culo actual.',
                excellent: 'Los valores de Trueque de venta limpios reflejan un veh&iacute;culo en la condici&oacute;n limpia. Esto significa un veh&iacute;culo sin defectos mec&aacute;nicos y pasa todas las inspecciones necesarias con la facilidad. La pintura, el cuerpo y las ruedas tienen la superficie menor que rasgu&ntilde;a con un fin de lustre alto y brillo. El interior refleja manchar m&iacute;nimo y ropa con todo el equipo en la orden de trabajo completa. El veh&iacute;culo tiene una historia de t&iacute;tulo limpia. El veh&iacute;culo necesitar&aacute; el reacondicionamiento m&iacute;nimo para ser preparado a la reventa. Como la condici&oacute;n de veh&iacute;culo individual var&iacute;a enormemente, los usuarios de NADAguides.com tendr&iacute;an que hacer ajustes independientes a la condici&oacute;n de veh&iacute;culo actual.'
            },
            equipment_title: 'Equipamiento opcional',
            equipment_text: 'Por favor seleccione las opciones inclu&iacute;das en su veh&iacute;culo.',
            equipment_total: 'Adiciones Total / Deducciones'
        },
        customer: {
            additional: {
                title: 'Informaci&oacute;n adicional',
                text: 'Su evaluaci&oacute;n del comercio est&aacute; listo! Por favor nos proporcione su informaci&oacute;n de contacto para que podamos enviarle un email de confirmaci&oacute;n de nuestra <strong>OFERTA ESPECIAL</strong>. Un informe de evaluaci&oacute;n detallada de su veh&iacute;culo se mostrar&aacute; en la p&aacute;gina siguiente'
            },
            contact: {
                title: 'Sus datos',
                fname: 'Nombre',
                lname: 'Apellido',
                email: 'Correo',
                phone: 'Tel&eacute;fono',
                zip: 'C&oacute;digo postal'
            },
            desired: {
                title: 'Deseada Informaci&oacute;n del Veh&iacute;culo',
                condition: 'Condicion',
                condition_options: {
                    newcar: 'Nuevo',
                    usedcar: 'Usado',
                    unsure: 'Inseguro'
                },
                category: 'Categor&iacute;a',
                all: "Todo",
                make: 'Marca',
                model: 'Modelo'
            },
            promotion: {
                title: 'Distribuidor Promoci&oacute;n!'
            },
            inventory: {
                title: 'Inventario Encontrado - <span class="match-count"></span> Veh&iacute;culo',
                text: '===MISSING TRANSLATION=== ===MISSING TRANSLATION=== ===MISSING TRANSLATION===',
                price: 'Precio',
                select: 'Seleccionar este veh&iacute;culo'
            }
        },
        review: {
            summary: {
                title: 'Resumen',
                text: 'Por favor imprima este resumen y llevarla con usted cuando nos visita. Una copia ha sido enviada a su direcci&oacute;n de e-mail y nuestro departamento de ventas.'
            },
            tradein: {
                title: 'Tu Comercio en el Veh&iacute;culo de Evaluaci&oacute;n',
                vehicle: 'Veh&iacute;culo:',
                mileage: 'Kilometraje:',
                zip: 'C&oacute;digo Postal:',
                range: 'B&aacute;sese Gama (<span name="condition"></span>):',
                opt_equip: 'Equipamiento Opcional:',
                mileage_adj: 'Ajuste de Kilometraje:',
                total: 'Suma',
                year: 'A&ntilde;o'
            },
            desired: {
                title: 'Su Deseado de Sustituci&oacute;n de veh&iacute;culos',
                vehicle: 'Veh&iacute;culo:',
                mileage: 'Kilometraje:',
                zip: 'C&oacute;digo Postal:',
                tradein_title: 'Comercio En el veh&iacute;culo',
                desired_title: 'El veh&iacute;culo deseado',
                compare: {
                    year: 'A&ntilde;o',
                    make: 'Marca',
                    model: 'Modelo',
                    body: 'Estilo'
                },
                total: 'Suma',
                calculate_pmt: 'Calcular el Pago'
            },
            contact: {
                title: 'Contacte con su Concesionario',
                dealership_info: 'Informaci&oacute;n de distribuci&oacute;n',
                appointment: 'Programar una Cita',
                directions: 'C&oacute;mo llegar',
                anytime: 'Cuando gustes'
            },
            directions: {
                title: 'C&oacute;mo llegar al Concesionario'
            },
            rules: {
                statement: {
                    title: 'Condiciones del Veh&iacute;culo Declaraci&oacute;n:',
                    text: 'El precio indicado refleja un veh&iacute;culo en la condici&oacute;n seleccionada. Costo de las reparaciones necesarias y reacondicionamiento (si es necesario) podr&iacute;a afectar el valor de tasaci&oacute;n final. Dependiendo de la condici&oacute;n del veh&iacute;culo y el equipo, el distribuidor puede ofrecer m&aacute;s o menos de su veh&iacute;culo.'
                },
                information: {
                    title: 'Informaci&oacute;n Importante:',
                    text: 'Por favor, pregunte a su distribuidor acerca de las posibles econom&iacute;as, aprovechando de los cr&eacute;ditos fiscales estatales a las ventas. Un representante de ventas se comunicar&aacute; con usted para m&aacute;s informaci&oacute;n. Esta estimaci&oacute;n se basa en la informaci&oacute;n proporcionada y no es una oferta de compra de su veh&iacute;culo. La actual oferta que usted recibe en su comercio en pueden ser m&aacute;s dependiendo de la condici&oacute;n de su veh&iacute;culo.'
                },
                privacy: {
                    title: 'Privacidad:',
                    text: 'Respetamos su derecho a la intimidad. Toda la informaci&oacute;n proporcionada ser&aacute; confidencial y no ser&aacute; compartida con ninguna otra parte.'
                },
                responsibility: {
                    title: 'Responsabilidad:',
                    text: 'Esta evaluaci&oacute;n es proporcionada por TradeInVelocity que es el &uacute;nico responsable de ella.'
                },
                expiration: {
                    title: 'Vencimiento:',
                    text: 'Tenga en cuenta que esta valoraci&oacute;n de veh&iacute;culos es v&aacute;lida por <span class="expiration-days"></span> d&iacute;as despu&eacute;s de la fecha de la evaluaci&oacute;n.',
                    text_desc: 'Expira en'
                }, 
            }
        },
        calc: {
            title: "Calculadora De Pagos",
            text: "Complete los campos siguientes para calcular su pago mensual.",
            label: {
                price: 'Veh&iacute;culo Precio:',
                apr: 'Tasa de Inter&eacute;s:',
                length: 'Duraci&oacute;n pr&eacute;stamo:',
                down_pmt: 'Pago:',
                trade_in: 'Comercio en valor:',
                total: 'Su Pago Mensual:'
            },
            disclaimer: 'Esta herramienta es s&oacute;lo para la estimaci&oacute;n y no representa una oferta que pueda ser aceptado por usted. P&oacute;ngase en contacto con su concesionario para obtener opciones de financiamiento.'
        },
        car: {
            label: {
                price: 'Precio:',
                city_mpg: 'MPG en la ciudad:',
                hwy_mpg: 'MPG en carretera:',
                exterior_color: 'Color Exterior:',
                interior_color: 'Color Interior:',
                stock_number: 'N�mero de Inventario:'
            }
        },
        upload: {
            image: {
                text: 'Puede cargar hasta 10 im&aacute;genes del veh&iacute;culo que actualmente posee. Una imagen de alta calidad no es obligatorio, pero se ayuda con una evaluaci&oacute;n m&aacute;s precisa!',
                label: 'Seleccione su archivo:',
                button: 'A&ntilde;adir archivo...',
                disclaimer: 'File Types: JPG, JPEG, PNG, BMP, GIF'+
                            '<br />Un m&aacute;ximo de 10 im&aacute;genes'+
                            '<br />L&iacute;mite de tama&ntilde;o: 2MB'
            },
            video: {
                text: 'Cargue video del veh&iacute;culo que actualmente posee directo a YouTube!',
                label: 'Seleccione su archivo:',
                button: 'A&ntilde;adir archivo...',
                disclaimer: '===MISSING TRANSLATION=== ===MISSING TRANSLATION=== ===MISSING TRANSLATION=== ===MISSING TRANSLATION=== ===MISSING TRANSLATION===',
                youtube_text: 'Ya tiene video en YouTube?',
                youtube_help: 'Complete el campo con el URL de su video YouTube.'
            }
        },
        validation: {
            missing: '===MISSING TRANSLATION===',
            phone_empty: '===MISSING TRANSLATION===',
            phone_format: '===MISSING TRANSLATION===',
            zip: '===MISSING TRANSLATION==='
        }
    }
    
    return lang; 
})
