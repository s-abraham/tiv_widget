define(['underscore', 'underscore.string', 'backbone', 'language'], function(_, _s, Backbone, Lang) {
    var validate = function(lang) {
        var errors = {};
        _.each(this.required, function(k) 
        {
            if(_.isUndefined(this.get(k)) || _.isNull(this.get(k)))
            {
                if (Lang != null && Lang[lang] != null) {
                    errors[k] = Lang[lang].validation.missing;
                }
            }
        }, this);
        return errors;
    };
    
    
    var models = {
        lang: Lang,
        tradein: Backbone.Model.extend({
            noImageRegExp: /noimage/gi,
            initialize: function() {
                this.on('change:mileage change:zipcode change:condition', this.__fetch_base, this);
                this.on('change:dealerkey', this.__save_dealerkey, this);
                this.on('change:appraisal', this.__set_pricing, this);
            },
            required: ['year', 'make', 'model', 'trim', 'mileage', 'condition'],
            validate_model: validate,
            __save_dealerkey: function (model, value, event) {                
                this.dealerkey = this.get('dealerkey');
            },
            __set_pricing: function(model, value, options) {
                var base_range = [];
                var range = [];
                var mileage_adj = 0;
                var options_adj = 0;

                _.each(value, function(apr) {
                    var value = _s.toNumber(apr.value);
                    value = isNaN(value) ? 0 : value;
                    switch(apr.name.toLowerCase())
                    {
                        case "highbase":
                            base_range[1] = value;
                            break;
                        case "lowbase":
                            base_range[0] = value;
                            break;
                        case "high":
                            range[1] = value;
                            break;
                        case "low":
                            range[0] = value;
                            break;
                        case "mileageadjust":
                            mileage_adj = value;
                            break;
                        case "optionadjust":
                            options_adj = value;
                            break;
                    }
                }, this);

                this.set('range', range);
                this.set('base_range', base_range);
                this.set('mileage_adj', mileage_adj);
                this.set('options_adj', options_adj);

            },
            __fetch_base: function (model, value, event) {
                
                if(_.isEmpty(this.validate_model()))
                {                   
                    var data = _.pick(this.toJSON(), this.required.concat('zipcode').concat('dealerkey'));
                    
                    this.fetch({
                        data: data,
                        success: _.bind(function() {
                            if(!_.isString(this.get('dealerkey')) && this.get('dealerkey').trim() === "")
                            {
                                this.set('dealerkey', this.dealerkey);
                            }
                            
                        }, this) 
                    })
                }
            },
            get_title: function(model, value, event)
            {
                if(_.isUndefined(this.get('title')))
                {
                   this.set('title', _s.sprintf('%s %s %s', 
                                                this.get('year'),
                                                this.get('make'),
                                                this.get('model')));
                }
                
                return this.get('title');
            },
            get_image: function(model, value, o)
            {
                if(_.isUndefined(this.get('image')))
                {
                    var image;
                    var images = this.get('images');
                    _.each(images, function(i) {
                        if(_.isUndefined(image) &&
                           !_.isNull(i['value']) && 
                           _s.trim(i['value']) != '') {
                            image = i['value'];
                        }
                    }, this);
                    
                    if(_.isUndefined(image) && (_.isArray(images) && !_.isUndefined(images[0])))
                    {
                        image = images[0]['value'];
                    }
                    this.set('image', image);
                }
                return this.get('image');
            },
            get_big_image: function(model, value, o)
            {
                if(_.isUndefined(this.get('big_image')))
                {
                    var image;
                    var images = this.get('images');
                    _.each(images, function(i) {
                        if(_.isUndefined(image) && !_.isNull(i['value']) && 
                           _s.trim(i['value']) != '' && 
                           (i['name']  == "ImageBigURL")) {
                            image = i['value'];
                        }
                    }, this);
                    
                    if(_.isUndefined(image) && (_.isArray(images) && !_.isUndefined(images[0])))
                    {
                        image = images[0]['value'];
                    }
                    this.set('big_image', image);
                }
                return this.get('big_image')
            },
            fetch_all: function () {
                var options = _.isArray(arguments[0]) ? arguments[0].join('|') : null;
                if(_.isEmpty(this.validate_model()))
                {                    
                    var data = _.pick(this.toJSON(), this.required.concat('zipcode').concat('dealerkey'));                    
                    
                    if(!_.isNull(options))
                    {
                        data.options = options;
                    }
                                       
                    this.fetch({
                        data: data,
                        success: _.bind(function() {
                            if(!_.isString(this.get('dealerkey')) && this.get('dealerkey').trim() == "")
                            {
                                this.set('dealerkey', this.dealerkey);
                            }
                        }, this) 
                    })
                }
            }
        }),
        upload: Backbone.Model.extend({
            
        }),
        equipment_option: Backbone.Model.extend({
            defaults: {
                selected: false
            }
        }),
        customer: Backbone.Model.extend({
            initialize: function() {
                this.on('change:phone1 change:phone2 change:phone3', this.__save_phone, this);    
            },
            __save_phone: function(model, value, event) {
                var phone = [model.get('phone1'), model.get('phone2'), model.get('phone3')];
                model.set('phone', phone);
            },
            defaults: {
                phone: [],
                lastname: null,
                firstname: null,
                zipcode: null,
                email: null
            },
            toJSON: function() {
                var o = Backbone.Model.prototype.toJSON.call(this);
                if(_.isArray(o.phone))
                {
                    o.phone = o.phone.join("");
                }
                return o;
            },
            regex: {
                zip: /[0-9]{5}/g,
                phone: /[0-9]{10}/g
            },
            required: ['firstname', 'lastname', 'email', 'zip'],
            validate_model: function(lang) {
                var errors = _.bind(validate, this)(lang),
                errMsg = models.lang[lang].validation;
                if(this.get('phone').length != 3)
                {
                    errors.phone = errMsg.phone_empty;
                }
                else if(_.isNull(this.get('phone').join('').match(this.regex.phone)))
                {
                    errors.phone = errMsg.phone_format;
                }
                if(_.isNull(String(this.get('zip')).match(this.regex.zip))) 
                {
                    errors.zip = errMsg.zip;
                }
                return errors;
            } 
        }),
        desired: Backbone.Model.extend({
            required: ['condition', 'make', 'model'],
            validate_model: function(lang) {
                if(this.get('condition') == 'unsure')
                {
                    return {};
                }
                return _.bind(validate, this)(lang);
            },
            defaults: {
                neworused: null
            }
        }),
        inventory: Backbone.Model.extend({
            initialize: function() {
                this.on('change:vehicles', this.__set_vehicles, this);
            },
            __set_vehicles: function(model, value, options) {
                this.collection.reset(value);
            }
        }),
        inventory_car: Backbone.Model.extend({
            initialize: function() {
                this.__set_photo();
                this.on('change:photoUrls', this.__set_photo, this);
            },
            __set_photo: function(m, v, o) {
                if(_.isUndefined(this.get('photoUrls')) || 
                    this.get('photoUrls').length == 0 || 
                    this.get('photoUrls')[0] == "")
                {
                    this.set('image', '<img src="img/NoImage.jpg" />');
                }
                else
                {
                    this.set('image', this.get('photoLightView'))
                }
            }
        }),
        promotion: Backbone.Model.extend({
            
        }),
        replacement: Backbone.Model.extend({
            initialize: function() {
                this.__set_photo();
                this.on('change:photoUrls', this.__set_photo, this);
            },
            __set_photo: function(m, v, o) {
                if(_.isUndefined(this.get('photoUrls')) || 
                    this.get('photoUrls').length == 0 || 
                    this.get('photoUrls')[0] == "")
                {
                    this.set('image', '<img src="img/NoImage.jpg" />');
                }
                else
                {
                    this.set('image', this.get('photoLightView'))
                }
            },
            get_image: function() {
                if(_.isUndefined(this.get('photoUrls')) || 
                    this.get('photoUrls').length == 0 || 
                    this.get('photoUrls')[0] == "")
                {
                    return '<img src="img/NoImage.jpg" />';
                }
                else
                {
                    return this.get('photoUrls')[0];
                }
            }
        }),
        appointment: Backbone.Model.extend({
            required: ['apptdate'],
            validate_model: validate,
            initialize: function() {
                this.on('change:phone', this.__join_phone, this);
                this.on('change:zip', this.__set_zipcode, this);
            },
            defaults: {
                appttime: "Anytime"                
            },
            __join_phone: function() {
                if(_.isArray(this.get('phone')))
                {
                    this.set('phone', this.get('phone').join(''), {silent: true});
                }
            },
            __set_zipcode: function() {
                this.set('zipcode', this.get('zip'));
            }
        }),
        appraisal: Backbone.Model.extend({
            
        }),
        pager: Backbone.Model.extend({
            defaults: {
                limit: 5,
                offset: 0,
                max: 10
            },
            next: function() {
                var offset = _s.toNumber(this.get('offset'))+_s.toNumber(this.get('limit'));
                if(offset >= _s.toNumber(this.get('max')))
                {
                    return;
                }
                this.set('offset', offset);
            },
            prev: function() {
                var offset = _s.toNumber(this.get('offset'))-_s.toNumber(this.get('limit'));
                if(offset < 0)
                {
                    offset = 0;
                }
                this.set('offset', offset);
            }
        }),
        directions: Backbone.Model.extend({
            required: ['from_address'],
            validate_model: validate
        }),
        calculator: Backbone.Model.extend({
            defaults: {
                price: 0,
                down_payment: 0,
                trade_in_value: 0,
                interest_rate: 6,
                loan_length: 24
            },
            initialize: function() {
                this.on('change', this.total, this);
            },
            total: function (m, o) {
                if(!_.isUndefined(m.changed['total']))
                {
                    return;
                }
                
                //Calculates the monthly payment asyncronously
                try
                {
                    var price = parseFloat(this.get('price') || 0) || 0;
                    var down_payment = parseFloat(this.get('down_payment') || 0) || 0;
                    var trade_in_value = parseFloat(this.get('trade_in_value') || 0) || 0;
                    var dp = down_payment + trade_in_value;
                    var ir = parseFloat(this.get('interest_rate') || 0) || 0;
                    var months = parseFloat(this.get('loan_length') || 0) || 0;
    
                    var pr = (ir / 100) / 12;
                    var larger_number = Math.pow((pr + 1), months);
                    var smaller_number = larger_number - 1;
                    var total = ((larger_number / smaller_number) * (price - down_payment - trade_in_value)) * pr;
    
                    this.set('total', total);
                }
                catch (Exception)
                {
                    this.set('total', finalNumber);
                }
            }
        }),
        dealership: {},
        widget_options: {}
    };
    return models;
});