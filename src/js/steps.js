/*
 * @author Freddy Knuth
 * 
 * Contains each of the Views for each of the steps 
 * initializes each of the views inside of the step along with 
 * the models and collections
 * 
 */

define(['require',
        'underscore',
        'jquery',
        'backbone',
        'views/appraisal',
        'jquery.uniform',
        'jquery.selectbox',
        'backbone.modelbinder'],
        function (require, _, jQuery, Backbone, appraisal) {
            //Show hide elements based on the scope of this.$el view            
            var show = function () {
                this.$el.show();
                //Doing the check for mobile browser at the moment as such to not skin the inputs on mobile
                if (!_.isUndefined(this["skin_input"]) && !jQuery.browser.mobile) {
                    this.skin_input();
                }
            };
            var hide = function () {
                this.$el.hide();
            }
            var show_errors = function (errors) {
                this.$('.error-block').remove();
                _.each(errors, function (message, k) {
                    var error = jQuery(document.createElement('span'));
                    error.addClass('error-block');
                    error.html("*" + message);

                    var input = this.$(':input[name=' + k + '],.input-' + k);

                    if (input[0].type == "radio") {
                        var row = input.parents('.condition-group:first');
                        row.append(error);
                    }
                    else {
                        var row = input.parents(':first');
                        row.append(error);
                    }

                }, this);
            }
            //Skin inputs
            var skin_input = function (el) {
                if (_.isUndefined(el)) {
                    //this.$(':input[type="radio"]:visible,:input[type="text"]:visible').uniform();
                    this.$(':input[type="text"],:input[type="tel",:input[type="number"]').uniform();
                    this.$('select:visible').selectBox();
                }
                else {
                    if (jQuery(el).find('select').length > 0) {
                        jQuery(el).find('select').selectBox();
                    }
                    else {
                        jQuery(el).selectBox();
                    }
                }
            }
            //Force page to top
            var scrollToTop = function () {
                $(window).scrollTop(0);
            }

            //Each of the steps views    
            var steps = {
                step1: Backbone.View.extend({
                    //Define the events for reset or next in the first step
                    skin_input: skin_input,
                    events: {
                        'click [data-toggle="reset"]': 'reset',
                        'click [data-toggle="next"]': 'next',
                        'click [data-toggle="language"]': 'switch_language',
                        'change input[type=radio]': 'onRadioClick' //,'click ul li': 'toggle'
                    },

                    onRadioClick: function (e) {
                        e.stopPropagation();
                        //this.model.tradein.set('condition',$(e.currentTarget).val());                       
                    },

                    show_equipment_price: true,
                    condition: true,
                    appraisal: Backbone.View.extend(appraisal),
                    initialize: function () {
                        //Extend the model and give it the URL
                        this.model.tradein = this.model.tradein.extend({
                            url: this.options.URL('tradein'),
                            defaults: {
                                condition: 'clean'
                            }
                        });

                        //Instantiate the model, make sure to update the Model object itself
                        this.model.tradein = new this.model.tradein({
                            dealerkey: this.options.dealerkey
                        });

                        //Instantiate the customer model
                        this.model.customer = new this.model.customer({
                            dealerkey: this.options.dealerkey
                        })

                        //Instatiate the options collection


                        this.collection.options = new this.collection.options([]);
                        this.collection.uploads = this.collection.uploads.extend({
                            url: this.options.URL('upload')
                        })

                        //Instatiate the options collection
                        this.collection.uploads = new this.collection.uploads([]);

                        //Bind the values of each of the fields to the correct models
                        var bindings = Backbone.ModelBinder.createDefaultBindings(this.el, 'name');
                        this.customerBinder = new Backbone.ModelBinder();

                        var zipBind = _.pick(bindings, 'zip');
                        this.customerBinder.bind(this.model.customer, this.$el, zipBind);

                        this.tradeinBinder = new Backbone.ModelBinder();
                        this.tradeinBinder.bind(this.model.tradein, this.$el, _.extend(_.omit(bindings, 'zip'), {
                            zipcode: zipBind.zip
                        }));

                        //Instatiate the trade in appraisal view
                        this.appraisal = new this.appraisal(_.extend({}, this.options, {
                            //Appraisal dropdowns only require the tradein appraisal model
                            model: this.model.tradein,
                            collection: this.collection.options,
                            el: this.$('.appraisal-panel'),
                            condition: this.condition
                        }));

                        this.show();

                        //Condition Modal
                        //this.condition_container = this.$('#tradein-condition');
                        //this.condition_template = _.template(_.unescape(this.condition_container.html()), null);
                        //this.condition_container.empty();

                        require(['views/equipment',
                                 'views/upload',
                                 'text!tpl/modal.upload.image.html',
                                 'text!tpl/modal.upload.video.html'], _.bind(this.init_views, this));
                    },
                    set_condition: function (condition) {
                        this.condition = condition;
                        this.appraisal.condition = condition;
                    },
                    init_views: function (equipment, upload, imageUpTPL, videoUpTPL) {
                        this.equipment = Backbone.View.extend(equipment);
                        this.upload = Backbone.View.extend(upload);

                        imageUpTPL = this.options.replace_language(imageUpTPL);
                        videoUpTPL = this.options.replace_language(videoUpTPL);

                        this.model.default_language.on('change', function () {
                            this.options.replace_language(imageUpTPL);
                            this.options.replace_language(videoUpTPL);
                        }, this);

                        this.equipment = new this.equipment(_.extend({}, this.options, {
                            collection: this.collection.options,
                            el: this.$('.equipment-panel')
                        }))

                        this.equipment.show_price = this.show_equipment_price;

                        this.appraisal.upload.image = new this.upload(_.extend({}, this.options, {
                            collection: this.collection.uploads,
                            el: imageUpTPL,
                            key: 'images'
                        }));

                        this.appraisal.upload.video = new this.upload(_.extend({}, this.options, {
                            collection: this.collection.uploads,
                            el: videoUpTPL,
                            key: 'videos'
                        }));

                        this.appraisal.on('clean', this.equipment.clean, this.equipment);
                        this.appraisal.on('clean', this.upload.clean, this.upload);

                    },
                    reset: function () {
                        location.reload();
                        //this.clean_options({
                        //    currentTarget : this.$('select[name="year"]')                    
                        //})
                    },
                    next: function () {
                        scrollToTop();

                        if (this.validate()) {
                            //If we have options, let's pull the full appraisal values with them
                            this.model.tradein.set('condition', $('input[name="condition"]:checked').val());
                            if (this.collection.options.length > 0) {
                                this.model.tradein.fetch_all(this.collection.options.get_selected('name'));
                            }

                            this.trigger('next', 1);
                            this.global_dispatcher.trigger('next', 1);
                        }
                    },
                    switch_language: function (e) {
                        var lang = jQuery(e.currentTarget).attr('data-language');

                        this.trigger('language:change', lang);
                        this.global_dispatcher.trigger('language:change', lang);
                    },
                    show: show,
                    hide: hide,
                    validate: function () {
                        
                        if ($("[name='trim']").val().toUpperCase() == 'SELECT TRIM') {
                            $('#trimerror').show();
                            return;
                        }

                        var validateModels = [this.model.tradein, this.model.customer];
                        var errors = {},
                        lang = this.model.default_language.get('language');

                        _.each(validateModels, function (model) {
                            _.extend(errors, model.validate_model(lang));
                        }, this)
                        errors = _.omit(errors, 'firstname', 'lastname', 'email', 'phone')
                        if (this.condition === false) {
                            errors = _.omit(errors, 'condition')
                        }
                        if (_.isEmpty(errors)) {
                            return true;
                        }
                        else {
                            _.bind(show_errors, this)(errors);
                        }
                    }
                }),
                step2: Backbone.View.extend({
                    skin_input: skin_input,
                    desired_form: true,
                    events: {
                        'click [data-toggle="back"]': 'back',
                        'click [data-toggle="next"]': 'next',
                        'click [data-toggle="reset"]': 'reset',
                        'click [data-trigger="select_vehicle"]': 'select_vehicle'
                    },
                    initialize: function () {
                        //Extend the model and give it the URL and dealerkey it needs
                        this.model.desired = this.model.desired.extend({
                            url: this.options.URL('desired')
                        })

                        //Instantiate the model, make sure to update the Model object itself
                        this.model.desired = new this.model.desired({ dealerkey: this.options.dealerkey, qmode: this.options.e_params.qmode, e_params: this.options.e_params });

                        //Instantiate the model, make sure to update the Model object itself
                        this.model.replacement = new this.model.replacement({ dealerkey: this.options.dealerkey, qmode: this.options.e_params.qmode, e_params: this.options.e_params });

                        if (_.isFunction(this.model.customer)) {
                            //Instantiate the model, make sure to update the Model object itself
                            this.model.customer = new this.model.customer({ dealerkey: this.options.dealerkey, qmode: this.options.e_params.qmode, e_params: this.options.e_params });
                        }


                        //Instantiate the collection, make sure to update the Model object itself
                        this.collection.inventory = new this.collection.inventory();

                        //Extend the model and give it the URL and dealerkey it needs
                        this.model.inventory = this.model.inventory.extend({
                            url: this.options.URL('inventory'),
                            collection: this.collection.inventory
                        })
                        //Instantiate the model, make sure to update the Model object itself
                        this.model.inventory = new this.model.inventory({ dealerkey: this.options.dealerkey });


                        //this.show();

                        //Lazy initialize once this step is initialized
                        require(['views/customer',
                                 'views/desired',
                                 'views/inventory'], _.bind(this.init_views, this));
                    },
                    init_views: function (customer, desired, inventory) {

                        this.customer = Backbone.View.extend(customer);
                        this.desired = Backbone.View.extend(desired);
                        this.inventory = Backbone.View.extend(inventory);

                        var bindings = Backbone.ModelBinder.createDefaultBindings(this.el, 'name');
                        var customerBindings = _.pick(bindings, 'firstname', 'lastname', 'email', 'phone1', 'phone2', 'phone3', 'zip')
                        this.customerBinder = new Backbone.ModelBinder();
                        this.customerBinder.bind(this.model.customer, this.$('.contact-form'), customerBindings);

                        var desiredBindings = _.omit(bindings, 'firstname', 'lastname', 'email', 'phone1', 'phone2', 'phone3', 'zip', 'replacement_vehicle')
                        this.desiredBinder = new Backbone.ModelBinder();
                        this.desiredBinder.bind(this.model.desired, this.$('.desired-form'), desiredBindings);

                        //WHY toJSON? it throws an error in older version of Android Browsers preventing the second page from working DM - 3/15/2013
                        this.customer = new this.customer(_.extend({}, this.options.toJSON, {
                            el: this.$('.contact-form')
                        }));

                        this.desired = new this.desired(_.extend({}, this.options.toJSON, {
                            model: this.model.desired,
                            el: this.$('.desired-form')
                        }))

                        this.inventory = new this.inventory(_.extend({}, this.options.toJSON, {
                            collection: this.collection.inventory,
                            model: this.model,
                            url: this.options.URL('inventory'),
                            el: this.$('.dealer-inventory')
                        }))

                        this.options.promotion.append_to(this.$('.promotion-container'));

                    },
                    back: function () {
                        location.reload();
                        //this.clean_options({
                        //    currentTarget : this.$('select[name="year"]')
                        //})
                    },
                    reset: function () {
                        location.reload();
                        //this.clean_options({
                        //    currentTarget : this.$('select[name="year"]')                    
                        //})
                    },
                    next: function () {

                        scrollToTop();
                        if (this.validate()) {
                            if (!_.isUndefined(this.inventory.modal)) {
                                this.inventory.modal.remove();
                            }
                            this.trigger('next', 2);
                            this.global_dispatcher.trigger('next', 2);
                        }
                    },
                    show: show,
                    hide: hide,
                    select_vehicle: function (e) {
                        var hcb = this.$('input[name="replacement_vehicle"]');
                        e.preventDefault();
                        _.each(hcb, function (el) {
                            jQuery(el).removeAttr('checked');
                        }, this)

                        jQuery(e.currentTarget).next().prop('checked', 'checked');
                        this.inventory.set_replacement_vehicle(e);
                        this.next();

                    },
                    validate: function () {
                        var validateModels = [this.model.customer, this.model.desired],
                        lang = this.model.default_language.get('language');
                        if (this.desired_form === false) {
                            validateModels = [this.model.customer]
                        }
                        var errors = {};
                        _.each(validateModels, function (model) {
                            _.extend(errors, model.validate_model(lang))
                        }, this)

                        if (_.isEmpty(errors)) {
                            return true;
                        }
                        else {
                            _.bind(show_errors, this)(errors);
                        }
                    }
                }),
                step3: Backbone.View.extend({
                    skin_input: skin_input,
                    events: {
                        'click [data-toggle="reset"]': 'reset'
                    },
                    initialize: function () {

                        //Extend the model and give it the URL and dealerkey it needs
                        this.model.appraisal = this.model.appraisal.extend({
                            url: this.options.URL('appraisal')
                        })
                        //Instantiate the model, make sure to update the Model object itself
                        this.model.appraisal = new this.model.appraisal({ dealerkey: this.options.dealerkey });

                        //Extend the model and give it the URL and dealerkey it needs
                        this.model.appointment = this.model.appointment.extend({
                            url: this.options.URL('appointment')
                        })
                        //Instantiate the model, make sure to update the Model object itself
                        this.model.appointment = new this.model.appointment({ dealerkey: this.options.dealerkey });

                        //Extend the model and give it the URL and dealerkey it needs
                        this.model.directions = this.model.directions.extend({
                        })
                        //Instantiate the model, make sure to update the Model object itself
                        this.model.directions = new this.model.directions();


                        this.model.appraisal.on('change:aprkey', function (m, v, o) {
                            this.set('aprkey', v);
                        }, this.model.appointment)

                        //Instantiate the model, make sure to update the Model object itself
                        this.model.calculator = new this.model.calculator();

                        this.model.tradein.on('change:range', function (m, v, o) {
                            this.set('trade_in_value', _.isArray(v) ? v[0] : v);
                        }, this.model.calculator);

                        this.model.replacement.on('change:price', function (m, v, o) {
                            this.set('price', v);
                        }, this.model.calculator);

                        //Lazy initialize once this step is initialized
                        require(['views/review',
                                 'views/appointment',
                                 'views/calculator',
                                 'text!tpl/modal.calculator.html',
                                 'text!tpl/email.html',
                                 'text!tpl/email.appointment.ENG.html',
                                 'text!tpl/email.appointment.ESP.html'], _.bind(this.init_views, this));
                    },
                    init_views: function (review, appointment, calculator, calculator_tpl, email_tpl, email_appt_eng_tpl, email_appt_esp_tpl) {
                        this.review = Backbone.View.extend(review);
                        this.appointment = Backbone.View.extend(appointment);
                        this.calculator = Backbone.View.extend(calculator);

                        this.review = new this.review(_.extend({}, this.options, {
                            el: this.$('.review-step'),
                            email_tpl: email_tpl
                        }))

                        this.appointment = new this.appointment(_.extend({}, this.options, {
                            el: this.$('.appointment-section'),
                            language_tpl: {
                                ENG: email_appt_eng_tpl,
                                ESP: email_appt_esp_tpl
                            }
                        }))


                        this.$('.promotion-container').append(this.options.promotion.model.attributes.ENG);
                        //this.options.promotion.append_to(this.$('.promotion-container'));
                        //this.$('.promotion-container').append(this.options.promotion.$el.clone());

                        var tpl = _.template(calculator_tpl, null, { variable: 'calc' });
                        tpl = tpl(this.model.calculator.toJSON());
                        //tpl = this.options.replace_language(tpl);

                        this.model.default_language.on('change', function () {
                            this.options.replace_language(tpl);
                        }, this);

                        this.calculator = new this.calculator(_.extend({}, this.options, {
                            el: tpl,
                            model: this.model.calculator
                        }));

                        this.review.calculator = this.calculator;

                        scrollToTop();

                        var myOpts = this.options.model.dealership.attributes.Options;

                        if (myOpts.EnableCreditApp === true) {
                            if (myOpts.CreditAppLink.toLowerCase().indexOf('http') < 0)
                                myOpts.CreditAppLink = 'http://' + myOpts.CreditAppLink;
                            $('#btn-creditapp').show().click(function () {
                                window.open(myOpts.CreditAppLink, '_blank');
                                return false;
                            });
                        }

                        if (myOpts.EnableNewInventory === true) {
                            if (myOpts.NewInventoryLink.toLowerCase().indexOf('http') < 0)
                                myOpts.NewInventoryLink = 'http://' + myOpts.NewInventoryLink;
                            $('#btn-newinv').show().click(function () {
                                window.open(myOpts.NewInventoryLink, '_blank');
                                return false;
                            });
                        }

                        if (myOpts.EnableUsedInventory === true) {
                            if (myOpts.UsedInventoryLink.toLowerCase().indexOf('http') < 0)
                                myOpts.UsedInventoryLink = 'http://' + myOpts.UsedInventoryLink;
                            $('#btn-usedinv').show().click(function () {
                                window.open(myOpts.UsedInventoryLink, '_blank');
                                return false;
                            });
                        }


                        if (this.options.e_params.qmode) {
                            var e_params = this.options.e_params;

                            this.model.desired.year = e_params.year;
                            this.model.desired.make = e_params.make;
                            this.model.desired.model = e_params.model;
                            this.model.desired.mileage = e_params.mileage;
                            this.model.desired.price = e_params.price;
                            this.model.desired.zipcode = e_params.dzip;
                            this.model.desired.body = '';

                            this.$('.desired-panel').removeClass('hide');
                            this.$('#dv-img').attr('src', e_params.imageurl);
                            this.$('#dv-year').html(e_params.year);
                            this.$('#dv-zip').html(e_params.dzip);
                            this.$('#dv-make').html(e_params.make);
                            this.$('#dv-make2').html(e_params.make);
                            this.$('#dv-model').html(e_params.model);
                            this.$('#dv-model2').html(e_params.model);
                            this.$('#dv-mileage').html(e_params.mileage);
                            this.$('#dv-price').html(e_params.price);

                            $('#cinfoName').html(e_params.dname);
                            $('#cinfoStreet').html(e_params.daddress);
                            $('#cinfoCity').html(e_params.dcity);
                            $('#cinfoState').html(e_params.dstate);
                            $('#cinfoPhone').html(e_params.dphone);
                            $('#cinfoEmail').html(e_params.demail);

                            $('.desired-panel').removeClass('hide');
                        }

                        setTimeout(function () { $('#condition-new').click(); }, 100);
                        setTimeout(function () { $('#condition-new').change(); }, 250);
                        setTimeout(function () { $('#category').change(); }, 500);

                    },
                    reset: function () {
                        location.reload();
                    },
                    show: function () {
                        if (this.model.tradein.attributes.base_range[0] == 0) {
                            $('#tvalsa1').hide();
                            $('#tvalsb1').show();
                        };
                        _.bind(show, this)();
                    },
                    hide: hide,
                    post_lead: function () {
                        this.review.post_lead();
                    }


                })
            }

            return steps;
        });