/**
 * @author Freddy Knuth
 */
require.config({
    baseUrl: './',
    deps : ['text', 'jquery', 'underscore', 'backbone', 'bootstrap', 'underscore.string'],
    paths : {
        //Base Libraries
        text : 'vendor/text',
        jquery : 'vendor/jquery', 
        underscore : 'vendor/underscore',
        backbone : 'vendor/backbone',
        bootstrap : 'vendor/bootstrap',
        'backbone.modelbinder' : 'vendor/Backbone.ModelBinder',
        'underscore.string' : 'vendor/underscore.string',
        
        //Jquery Plugins
        'jquery-mobile-detect': 'vendor/jquery-mobile-detect',
        'jquery-ui-datepicker': 'vendor/jquery-ui-datepicker',
        'jquery.ui.widget': 'vendor/jquery.fileupload/jquery.ui.widget',
        'jquery.selectbox' : 'vendor/jquery.selectBox',
        'jquery.uniform' : 'vendor/jquery.uniform',
        'jquery.flash' : 'vendor/jquery.flash',
        'jquery.colorbox': 'vendor/jquery.colorbox',
        'jquery.jqprint': 'vendor/jquery.jqprint',
        'jquery.cookie': 'vendor/jquery.cookie',
        
        //Fileupload Plugins
        'jquery.fileupload': 'vendor/jquery.fileupload/jquery.fileupload',
            'tmpl': 'vendor/jquery.fileupload/tmpl',
            'load-image': 'vendor/jquery.fileupload/load-image',
            'canvas-to-blob': 'vendor/jquery.fileupload/canvas-to-blob',
            'jquery.fileupload-fp': 'vendor/jquery.fileupload/jquery.fileupload-fp',
            'jquery.fileupload-ui': 'vendor/jquery.fileupload/jquery.fileupload-ui',

        //Google Maps
        'gmap3' : 'vendor/gmap3'

        
        
    },
    shim : {
        'underscore' : {
            exports : '_'
        },
        'underscore.string' : {
            deps : ['underscore'],
            exports : '_s'
        },
        'backbone' : {
            deps : ['jquery', 'underscore', 'underscore.string'],
            exports : 'Backbone'
        },
        'backbone.modelbinder' : {
            deps : ['backbone']
        },
        'jquery' : {
            exports : 'jQuery'
        },
        'jquery-ui-datepicker': ['jquery'],
        'jquery.fileupload' : ['jquery',
                               'jquery.ui.widget',
                               'vendor/jquery.fileupload/jquery.iframe-transport'],
                               
        'jquery.fileupload-ui': ['jquery.fileupload'],
        'jquery.fileupload-fp': ['jquery.fileupload'],
                               
        'jquery.flash': ['jquery'],
        'gmap3': ['jquery'],
        'jquery.cookie': ['jquery'],
        'jquery.selectbox' : ['jquery'],
        'jquery.uniform' : ['jquery'],
        'bootstrap' : ['jquery']
    }
})