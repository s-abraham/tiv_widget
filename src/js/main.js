
define(['require',
        'underscore',
        'backbone',
        'steps',
        'language',
        'collections',
        'text!tpl/step1.html'],
        //'bootstrap',
        //'jquery.flash'], 
        function (require, _, Backbone, StepViews, Lang, Collection, Step1TPL) {
            //Defaults
            jQuery.ajaxSetup({
                dataType: 'json',
                global: false,
                type: "GET"
            });


            (function (Backbone) {
                var dispatcher;
                if (this.isExtended) {
                    return;
                }
                dispatcher = _.extend({}, Backbone.Events, {
                    cid: "dispatcher"
                });
                return _.each([Backbone.Collection.prototype, Backbone.Model.prototype, Backbone.View.prototype, Backbone.Router.prototype], function (proto) {
                    return _.extend(proto, {
                        global_dispatcher: dispatcher
                    });
                });
            })(Backbone);

            //Get properties of an object defined by a string path of parent.child.child      
            var getProp = function (obj, path) {
                if (!_.isArray(path)) { path = path.split('.') }
                if (!_.isObject(obj) || path.length == 0) {
                    return obj;
                }
                if (obj[path[0]]) {
                    return getProp(obj[path[0]], path.slice(1));
                }
                return;
            }

            var Widget = {
                //debug 
                events: {
                    'click [data-toggle="switch-view"]': 'switcher',
                },
                Language: Lang,
                Step: {
                    step1: Step1TPL,
                    step2: false,
                    step3: false
                },
                Video: {
                    params: {
                        quality: 'autohigh',
                        loop: 'false',
                        wmode: 'transparent',
                        bgcolor: '#ffffff',
                        allowScriptAccess: '#sameDomain',
                        width: 200,
                        height: 360
                    },
                    URL: {
                        ENG: 'http://laserstreamvideo.com/clients/GAA/GAA-2POD.swf',
                        ESP: 'http://laserstreamvideo.com/clients/GAA/GAA-Spanish-2.swf'
                    }
                },
                default_lang: 'ENG',
                winWidth: 0,
                winHeight: 0,

                initialize: function () {

                    //Set up resize event, this is specifically done to address issues with selectBox redraw - we will poll and fire the function after done resizing - DM 3/6/2013
                    var fireRedraw;
                    $(window).on('resize', this.redrawSelects);

                    //Set listeners or check against the widget options from dealership info
                    this.init_widget_options();

                    //Check for the dealerkey
                    if (_.isUndefined(this.options.dealerkey)) {
                        document.write('No dealerkey provided');
                        throw Error('No Dealerkey provided')
                    }
                    //Check for the dealerkey to at least be a string 
                    if (!_.isString(this.options.dealerkey)) {
                        document.write('Invalid dealerkey provided');
                        throw Error('Invalid Dealerkey provided')
                    }
                    //Set the dealerkey/params class level
                    this.dealerkey = this.options.dealerkey;
                    this.e_params = this.options.e_params;


                    //Set the language model and its listener
                    this.default_language = new Backbone.Model();
                    this.default_language.on('change', this.render_language, this);


                    //Check for global events of language change
                    this.global_dispatcher.on('language:change', this.set_language, this);

                    //Check for global events of the next step
                    this.global_dispatcher.on('next', this.change_step, this);


                    //Set the URL function to use for all requests
                    this.URL = this.options.URL;

                    //Set the APR key on the appraisal model (Deprecated)
                    //this.model.appraisal.on('change:aprkey', this.set_aprkey, this);

                    //Extend the model and give it the URL and dealerkey it needs

                    //Instantiate the model, make sure to update the Model object itself
                    this.model.promotion = new this.model.promotion({
                        dealerkey: this.options.dealerkey,
                        e_params: this.options.e_params,
                        'ENG': this.model.dealership.get('SpecialOfferHTML'),
                        'ESP': this.model.dealership.get('SpecialOfferHTML_ESP'),
                        hasimage: this.model.dealership.get('SpecialOfferHasImage')
                    });
                    this.model.default_language = this.default_language;
                    this.options.replace_language = _.bind(this.replace_language, this);

                },
                init_widget_options: function () {
                    //If the Nada branding is enabled, show the NADA Logo
                    this.model.widget_options.on('change:EnableNADABranding', function (model) {
                        if (model.get('EnableNADABranding') !== true) {
                            jQuery('.nada-logo').removeClass('hide');
                        }
                    }, this);

                    //Set the default language based on what is coming from the dealer 
                    this.model.widget_options.on('change:Language', function (model) {
                        this.default_language.set({ 'language': model.get('Language') })

                    }, this);


                    //Show or not the video introduction
                    this.model.widget_options.on('change:EnableVideo', function (model) {
                        if (model.get('EnableVideo')) {
                            this.show_video('step1', model.get('Language'))
                        }
                        else {
                            this.$('.widget-content .step-video#step1-video').hide().remove();
                        }

                    }, this)


                    this.on('loaded', this.check_options, this);
                },
                check_options: function () {
                    var options = this.model.widget_options;
                    if (options.get('SocialMedia') === true) { }

                    if (options.get('EnableInstantOptionPricing') !== true) {
                        this.$('.widget-content .options-total-row').hide();
                        this.Step.step1.show_equipment_price = false;
                    }

                    if (options.get('EnableTradeInCondition') !== true) {
                        this.$('.tradein-condition-row').hide();
                        this.Step.step1.set_condition(false);
                    }

                    if (options.get('EnableImageUpload') !== true || this.options.e_params.qmode) {
                        this.$('.action-image-upload').hide()
                    }
                    if (this.options.e_params.qmode) {                        
                        $('#appointment-section').hide()
                        $('#dealer-contact').hide()
                    }

                    if (options.get('EnableVideoUpload') !== true || this.options.e_params.qmode) {
                        this.$('.action-video-upload').hide()
                    }

                    if (options.get('EnableTradeIn') !== true || this.options.e_params.qmode) {
                        this.$('.desired-form').hide();
                        this.Step.step2.desired_form = false;
                    }

                    if (options.get('EnableNewOption') !== true) {
                        this.$('.condition-group .condition-new').hide();
                    }

                    if (options.get('EnableUnsureOption') !== true) {
                        this.$('.condition-group .condition-unsure').hide();
                    }

                    if (options.get('EnablePaymentCalculator') !== true) {
                        this.$('.calculator-row').hide();
                    }

                    if (options.get('EnableAdditionalStep') !== true) {

                    }

                    if (options.get('EnableNewInventory') !== true) {

                    }

                    if (options.get('EnableUsedInventory') !== true) {

                    }

                    if (options.get('EnableSettlementAmount') !== true) {

                    }
                    if (options.get('EnableMonthlyPayment') !== true) {

                    }

                    if (options.get('GoogleUrchinCode') !== true) {

                    }

                    //iFrame Communication functionality - DM 03/05/2013
                    if (window['tiv']) { tiv.afterRender(); };

                    //Place Holder for Footer Dealer Name Customization
                    this.personalize();

                },
                //Initialize the first step and load the other 2
                init_steps: function () {
                    //Get the templates for step 2 and 3
                    require(['text!tpl/step2.html', 'text!tpl/step3.html', 'views/promotion'], _.bind(this.complete_steps, this))

                    this.setThemes();
                    //Append the template for step1
                    var step1 = jQuery(this.Step.step1).hide();
                    this.$('.widget-content').append(step1);

                    //Initialize the first step 
                    this.Step.step1 = new StepViews.step1(_.extend({}, this.options, {
                        el: step1
                    }));
                    //Set the currentView
                    this.currentStep = this.Step.step1;

                },
                //Complete the load of the last 2 steps
                complete_steps: function (step2, step3, promotion) {
                    this.promotion = Backbone.View.extend(promotion),

                    //Set the enabled flag
                    this.model.promotion.set('enabled', this.model.widget_options.get('EnableSpecialOffer'));

                    //Instatiate the promotion view immediately
                    this.promotion = new this.promotion(_.extend({}, _.omit(this.options, 'el'), {
                        model: this.model.promotion
                    }))

                    this.default_language.on('change', function () {
                        this.model.promotion.set('language', this.default_language.get('language'));
                    }, this);

                    this.options.promotion = this.promotion;

                    //Hide the steps immediately
                    step2 = jQuery(step2).hide();
                    step3 = jQuery(step3).hide();

                    //Append them to the container for the steps
                    this.$('.widget-content').append(step2).append(step3);

                    //Init step 2
                    this.Step.step2 = new StepViews.step2(_.extend({}, this.options, {
                        el: step2
                    }))

                    //Init step 3
                    this.Step.step3 = new StepViews.step3(_.extend({}, this.options, {
                        el: step3
                    }));

                    //Trigger language change so that the steps are translated
                    this.default_language.trigger('language:change', this.default_language, this.default_language.get('language'));
                    this.trigger('loaded');
                },
                personalize: function () {
                    //Prevents personalization from firing multiple times during localization calls
                    var self = this;
                    if (this.firePersonalize) { clearTimeout(this.firePersonalize); }
                    this.firePersonalize = setTimeout(function () {
                        var d = new Date(),
                            y = d.getFullYear(),
                            dealer = self.model.dealership.get('DealerName');
                        jQuery('#current-year').html(y);
                        jQuery('#dealership-name').html(dealer);
                    }, 100);
                },
                setThemes: function () {
                    var options = this.model.widget_options;
                    switch (options.get('Theme')) {
                        case 'Red':
                            $('#CSSTheme').attr('href', 'css/themes/default.css');
                            break;
                        case 'Blue':
                            $('#CSSTheme').attr('href', 'css/themes/BlueGray.css');
                            break;
                        case 'Green':
                            $('#CSSTheme').attr('href', 'css/themes/GreenGray.css');
                            break;
                        case 'Dark_Gray':
                            $('#CSSTheme').attr('href', 'css/themes/DarkGray.css');
                            break;
                        case 'Yellow':
                            $('#CSSTheme').attr('href', 'css/themes/YellowGray.css');
                            break;
                        case 'YellowBlackBg':
                            $('#CSSTheme').attr('href', 'css/themes/YellowGrayBlackBg.css');
                            break;
                        case 'Yellow_Gray':
                            $('#CSSTheme').attr('href', 'css/themes/YellowGrayGrayBg.css');
                            break;
                        case 'RedBlackBG':
                            $('#CSSTheme').attr('href', 'css/themes/RedGrayBlackBG.css');
                            break;
                        case 'BMW':
                            $('#CSSTheme').attr('href', 'css/themes/BMW.css');
                            break;
                        case 'Purple':
                            $('#CSSTheme').attr('href', 'css/themes/Purple.css');
                            break;
						case 'Carsoup':
	                    $('#CSSTheme').attr('href', 'css/themes/Carsoup.css');
	                    break;
                    };
                },
                //Deprecated
                set_aprkey: function (model) {
                    var aprkey = model.get('aprkey');
                    _.each(this.model, function (model) {
                        model.set('aprkey', aprkey, { silent: true });
                    }, this);
                },
                //Set the default language
                set_language: function (lang) {
                    var el = jQuery(this.currentStep.$el);
                    this.default_language.set('language', lang);
                },
                //Redraw select boxes on Resize to fit the space
                redrawSelects: function () {
                    //New height and width
                    var winNewWidth = $(window).width(),
                    winNewHeight = $(window).height();

                    // compare the new height and width with old one
                    if (winNewWidth > this.winWidth + 25 || winNewWidth < this.winWidth - 25 ||
                       winNewHeight > this.winHeight + 25 || winNewHeight < this.winHeight - 25) {
                        if (this.fireRedraw) { clearTimeout(this.fireRedraw); }
                        this.fireRedraw = setTimeout(function () {
                            this.$('select').selectBox('destroy');
                            this.$('select').selectBox();
                        }, 500);
                    }
                    //Update the width and height
                    winWidth = winNewWidth;
                    winHeight = winNewHeight;
                },
                //Replace all the text nodes with language attributes to the desired language
                render_language: function () {

                    this.replace_language(this.$el)

                    if (this.$('.widget-content .step-video#step1-video:visible').length > 0) {
                        this.show_video('step1', lang);
                    }
                },
                replace_language: function (el) {
                    el = jQuery(el);
                    var lang = this.default_language.get('language');
                    var languageObj = this.Language[lang];

                    //Clean up any errors
                    this.$('span.error-block').remove();

                    _.each(el.find('[data-lang]'), function (el) {
                        el = jQuery(el);
                        var str = getProp(this, el.attr('data-lang'));
                        if (str) {
                            el.html(str);
                        }
                    }, languageObj);

                    this.$('select').selectBox('refresh');
                    this.personalize();

                    return el;
                },
                show_video: function (step, lang) {
                    if (this.$('.widget-content .step-video#' + step + '-video:visible').length > 0) {
                        //Make sure jquery flash is loaded
                        require(['jquery.flash'], _.bind(function () {
                            var params = _.defaults({
                                src: this.Video.URL[lang]
                            }, this.Video.params);

                            this.$('.widget-content .step-video#' + step + '-video').empty().flash(params);
                        }, this));
                    }
                },
                //Change to the desired step
                change_step: function (step) {
                    switch (step) {
                        case 0:
                            this.switch_views(this.Step.step1);
                            this.currentStep = this.Step.step1;
                            break;
                        case 1:
                            this.switch_views(this.Step.step2);
                            this.currentStep = this.Step.step2;
                            break;
                        case 2:
                            this.switch_views(this.Step.step3);
                            this.currentStep = this.Step.step3;
                            this.Step.step3.post_lead();
                            break;
                    }
                },
                //Switch views by hiding all the other steps and showing requested one
                switch_views: function (view) {
                    _.each(this.Step, function (view, k) {
                        if (view)
                            view.hide();
                    }, this)
                    if (view)
                        view.show();
                },
                //Debug Only
                switcher: function (e) {
                    var step = parseInt(jQuery(e.currentTarget).attr('data-step'));
                    this.change_step(step);
                }
            }

            Widget = Backbone.View.extend(Widget);

            return Widget;
        })
