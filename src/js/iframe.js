/* 
 * a backwards compatable implementation of postMessage
 * by Josh Fraser (joshfraser.com)
 * released under the Apache 2.0 license.  
 *
 * this code was adapted from Ben Alman's jQuery postMessage code found at:
 * http://benalman.com/projects/jquery-postmessage-plugin/
 * 
 * other inspiration was taken from Luke Shepard's code for Facebook Connect:
 * http://github.com/facebook/connect-js/blob/master/src/core/xd.js
 *
 * the goal of this project was to make a backwards compatable version of postMessage
 * without having any dependency on jQuery or the FB Connect libraries
 *
 * my goal was to keep this as terse as possible since my own purpose was to use this 
 * as part of a distributed widget where filesize could be sensative.
 * 
 */

// everything is wrapped in the XD function to reduce namespace collisions
var XD = function(){

    var interval_id,
    last_hash,
    cache_bust = 1,
    attached_callback,
    window = this;

    return {
        postMessage : function(message, target_url, target) {

            if (!target_url) {
				return;
            }
            target = target || parent;  // default to parent

            if (window['postMessage']) {
                // the browser supports window.postMessage, so call it with a targetOrigin
                // set appropriately, based on the target_url parameter.
                
                target['postMessage'](message, target_url.replace( /([^:]+:\/\/[^\/]+).*/, '$1'));

            } else if (target_url) {
                // the browser does not support window.postMessage, so set the location
                // of the target to target_url#message. A bit ugly, but it works! A cache
                // bust parameter is added to ensure that repeat messages trigger the callback.
                target.location = target_url.replace(/#.*$/, '') + '#' + (+new Date()) + (cache_bust++) + '&' + message;
            }
        },

        receiveMessage : function(callback, source_origin) {

            // browser supports window.postMessage
            if (window['postMessage']) {
                // bind the callback to the actual event associated with window.postMessage
                if (callback) {
                    attached_callback = function(e) {
                        if ((typeof source_origin === 'string' && e.origin !== source_origin) ||
						(Object.prototype.toString.call(source_origin) === "[object Function]" && source_origin(e.origin) === !1)) {
                            return !1;
                        }
                        callback(e);
                    };
                }
                if (window['addEventListener']) {
                    window[callback ? 'addEventListener' : 'removeEventListener']('message', attached_callback, !1);
                } else {
                    window[callback ? 'attachEvent' : 'detachEvent']('onmessage', attached_callback);
                }
            } else {
                // a polling loop is started & callback is called whenever the location.hash changes
                interval_id && clearInterval(interval_id);
                interval_id = null;

                if (callback) {
                    interval_id = setInterval(function(){
                        var hash = document.location.hash,
                        re = /^#?\d+&/;
                        if (hash !== last_hash && re.test(hash)) {
                            last_hash = hash;
                            callback({data: hash.replace(re, '')});
                        }
                    }, 100);
                }
            }
        }
    };
}();

/*
Dynamic height adjustment for the iFrame is possible if using a polling type plugin i.e. http://benalman.com/projects/jquery-resize-plugin/
that monitors the height of the body of the widget inside the iFrame. We could alternatively add a bunch of calls inside the widget
backbone app that fires after an action or section a shows (not recommended). There doesnt seem to be a need for this functionality at the time being as
the height is being hardcoded. It is possible to pass in the height as part of the tiv.init call.
*/

var  tiv = (function (){
	var delta;
	var defaults = {
		//url : 'http://dev.tivremote.com',
		resize: false,
		tivWin: 'tivEMBED',
		padding: 20
	};

	//Generic Object Merging 
	var settings = function(options){
		for(var i in options){
			try{
				if(options[i].constructor == Object){
					//Recurse
					defaults[i] = settings(options[i], defaults[i]);
				}else{
					//Merge
					if(options[i] !== undefined){ defaults[i] = options[i]; }
				}
			}catch(e){
				//Add
				defaults[i] = options[i];
			}
		}
		return defaults;
	};

	return {

		queryString : function(qs, key){
			key = key.replace(/[\[]/,'\\\[').replace(/[\]]/,'\\\]');
			var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
			qs = regex.exec(qs);
			return ((qs !== null)? qs[1] : null );
		},

		parseDomain : function(url){
			var match = url.match(/(http(s)?):\/\/[a-zA-Z0-9-.]+/),
			val = ((match)? match[0] : null);
			return val;
		},

		init : function(options) {
			
			var s = settings(options),
				scroll = ((s.resize)? 'auto' : s.resize),
				origin_domain = ((s.url)? this.parseDomain(s.url) : '');

			//Set Receiver - Waiting for remote call
			XD.receiveMessage(function(message){
				if (message.origin && message.origin !== origin_domain.toString()) { return; }  
				if(message.data.indexOf('|') != -1){
					var hw = message.data.split('|');

					document.getElementById(s.tivWin).style.height = hw[0]+'px';
					//document.getElementById(s.tivWin).style.width = hw[1]+'px';
				}
			}, origin_domain);

			//Write the iframe to the page
			document.open();
			document.write('<iframe id="'+s.tivWin+'" scrolling="'+scroll+'" src="'+s.url+'#'+encodeURIComponent(document.location.href)+'" allowTransparency="true" frameborder="0" border="0" style="min-width:320px; width:80%">');
			document.close();

			//setTimeout(function(){
			//	XD.postMessage('IFRAME ATTACHED', 'http://dev.tivremote.com', document.getElementById(s.tivWin).contentWindow);
			//}, 50)

		},
		afterRender: function(){
			// Called within the Backbone App and only if located within an iFrame
			if ( window.self !== window.top ) {
				var db = document.body,
				dde = document.documentElement,
				docHeight = ((!dde)? Math.max(db.scrollHeight, db.offsetHeight, db.clientHeight) : Math.max(dde.scrollHeight, dde.offsetHeight, dde.clientHeight)),
				docWidth  = ((!dde)? Math.max(db.scrollWidth, db.offsetWidth, db.clientWidth) : Math.max(dde.scrollWidth, dde.offsetWidth, dde.clientWidth)),
				parentURL = this.parseDomain(decodeURIComponent(document.location.hash.replace(/^#/, '')));

				

				// Why do we do this? We need to determine if someone resizes down (smaller) then up (larger) clientHeight will grow but not shrink after resize
				if(delta){
					if(docWidth > delta){
						docHeight = ((!dde)? Math.min(db.scrollHeight, db.offsetHeight, db.clientHeight) : Math.min(dde.scrollHeight, dde.offsetHeight, dde.clientHeight));
						
					}else{
						
					}
				}

				message = docHeight + '|' + docWidth;

				

				XD.postMessage(message, parentURL);

				//Set Receiver On Remote
				//XD.receiveMessage(function(message){
				//	alert('REMOTE = ' + message.data);
				//}, parentURL);

				delta = docWidth;
			}

		}
	};
}());

//Should only run in the parent
var tivP = (function(){
		if (!window.postMessage||!window.addEventListener) {
			window.attachEvent('onresize', function(){ 
				tiv.afterRender();
			});
			//Specific for Mobile Browsers as many dont support resize properly
			window.attachEvent('onload', function(){ 
				tiv.afterRender();
			});
		}else{
			window.addEventListener('resize', function(){
				tiv.afterRender();
			}, false);
			//Specific for Mobile Browsers as many dont support resize properly
			window.addEventListener('load', function(){
				tiv.afterRender();
			}, false);
		}
}());


