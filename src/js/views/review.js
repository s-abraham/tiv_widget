
define(['jquery',
        'underscore',
        'underscore.string',
        'backbone',
        'text!tpl/modal.html',
        'bootstrap',
        'jquery.selectbox',
        'jquery.jqprint',
        'gmap3',
        'backbone.modelbinder'], function (jQuery, _, _s, Backbone, Modal) {
            var show_errors = function (errors) {
                this.$('.error-block').remove();
                _.each(errors, function (message, k) {
                    var error = jQuery(document.createElement('span'));
                    error.addClass('error-block');
                    error.html("*" + message);

                    var input = this.$(':input[name=' + k + '],.input-' + k);

                    if (input[0].type == "radio") {
                        var row = input.parents('.condition-group:first');
                        row.append(error);
                    }
                    else {
                        var row = input.parents(':first');
                        row.append(error);
                    }

                }, this);
            }
            return {
                events: {
                    'click .util-icon-block [data-toggle="print"]': 'print',
                    'click .util-icon-block [data-toggle="print_directions"]': 'print_directions',
                    'click [data-trigger="directions"]': 'pre_directions',
                    'click [data-trigger="calculator"]': 'show_calculator',
                    //'click [data-trigger="email"]': 'post_lead',
                    'click [data-trigger="save_tpl"]': 'save_tpl'

                },
                modal_template: _.template(Modal, null, { variable: 'modal' }),
                model_to_post_map: {
                    tradein: ['year', 'make', 'model', 'trim', 'mileage', 'condition'],
                    customer: {
                        'lastname': 'lastname',
                        'firstname': 'firstname',
                        'zip': 'zipcode',
                        'email': 'email',
                        'phone': 'phone'
                    },
                    desired: {
                        'condition': 'desiredcondition',
                        'category': 'category',
                        'make': 'desiredmake',
                        'model': 'desiredmodel'
                    },
                    replacement: {
                        'mileage': 'desiredmileage',
                        'year': 'desiredyear',
                        'trim': 'desiredtrim',
                        'color': 'desiredcolor',
                        'neworused': 'neworused',
                        'vin': 'selected'
                    }
                },
                URL: {
                    dealership: '/tivws/api/dealer/'
                },
                save_tpl: function () {
                    //Insntatiate the email template
                    this.email_template = _.template(_.unescape(this.$(':input[name=emailtpl]').val()), null, { variable: 'lead' });

                },
                format:
                {
                    range: function (value, rangeEnabled) {
                        if (!_.isArray(value)) {
                            value = ["0", "0"];
                        }
                        var value1 = _s.toNumber(value[0]);
                        value1 = isNaN(value1) ? 0 : value1;
                        var value2 = _s.toNumber(value[1]);
                        value2 = isNaN(value2) ? 0 : value2;

                        if (rangeEnabled === true) {
                            var retValue = _s.sprintf("$%s to $%s", _s.numberFormat(value1, 0), _s.numberFormat(value2, 0));
                        }
                        else {
                            var retValue = _s.sprintf("$%s", _s.numberFormat(value1, 0));
                        }
                        return retValue;
                    },
                    currency: function (value) {
                        var number = _s.toNumber(value);
                        number = isNaN(number) ? 0 : number;
                        var format = "$%s";
                        if (number < 0) {
                            number *= -1;
                            format = "($%s)"
                        }

                        return _s.sprintf(format, _s.numberFormat(number, 0));
                    }

                },
                converter: {
                    range: function (direction, value) {
                        if (direction == "ModelToView" && _.isArray(value)) {
                            return this.format.range(value, this.model.widget_options.get('EnableValueRanges'))
                        }
                        return value;
                    },
                    currency: function (direction, value) {
                        if (direction == "ModelToView") {
                            return this.format.currency(value)
                        }
                        return value;
                    },
                    number: function (direction, value) {
                        if (direction == "ModelToView") {
                            return _s.numberFormat(_s.toNumber(value), 0);
                        }
                        return value;
                    }
                },
                initialize: function () {
                    this.dealerkey = this.options.dealerkey;

                    //Insntatiate the email template
                    this.email_template = _.template(this.options.email_tpl, null, { variable: 'lead' });


                    //Create bindings and add the respective converters to the specific fields in the trade in panel
                    var tradeinBindings = Backbone.ModelBinder.createDefaultBindings(this.$('.tradein-panel'), 'name');
                    tradeinBindings.mileage.converter = this.converter.number;
                    tradeinBindings.range.converter = tradeinBindings.base_range.converter = _.bind(this.converter.range, this);
                    tradeinBindings.mileage_adj.converter = tradeinBindings.options_adj.converter = _.bind(this.converter.currency, this);
                    this.tradeinBinder = new Backbone.ModelBinder();
                    this.tradeinBinder.bind(this.model.tradein, this.$('.tradein-panel'), tradeinBindings)

                    this.model.tradein.on('change:image', this.__set_tradein_image, this);
                    this.collection.options.on('change', this.__draw_options, this);

                    //Bind customer information in the tradein panel to the customer model
                    this.customerBinder = new Backbone.ModelBinder();
                    this.customerBinder.bind(this.model.customer, this.$('.tradein-panel'))

                    //Get the default desiredPane bindings, this is due to trade in and desired sharing similar fields
                    var desiredPaneBindings = Backbone.ModelBinder.createDefaultBindings(this.$('.desired-panel'), 'name');
                    //Clone the bindings and append their selector with the data-source desired attribute
                    var desiredBindings = _.clone(desiredPaneBindings);
                    _.each(desiredBindings, function (bind, k) {
                        this[k] = _.clone(bind);
                        if (k == "zipcode" || k == "trim") {
                            delete this[k];
                            return;
                        }
                        this[k].selector += '[data-source="desired"]';
                    }, desiredBindings);
                    desiredBindings.mileage.converter = _.bind(this.converter.number, this);
                    desiredBindings.price.converter = _.bind(this.converter.currency, this);
                    this.desiredBinder = new Backbone.ModelBinder();
                    this.desiredBinder.bind(this.model.replacement, this.$('.desired-panel'), desiredBindings);
                    this.model.replacement.on('change:image', this.__set_desired_image, this);


                    //Clone the bindings and append their selector with the data-source tradein attribute
                    var tradeinBindings = _.clone(desiredPaneBindings);
                    _.each(tradeinBindings, function (bind, k) {
                        this[k] = _.clone(bind);
                        if (k == "mileage" || k == "price" || k == "body") {
                            delete this[k];
                            return;
                        }
                        this[k].selector += '[data-source="tradein"]';
                    }, tradeinBindings);
                    this.desiredTradeinBinder = new Backbone.ModelBinder();
                    //Bind the tradein model to the desired panel based on the data source selectors
                    this.desiredTradeinBinder.bind(this.model.tradein, this.$('.desired-panel'), tradeinBindings);

                    //Show the desired panel if we have data for it
                    this.model.replacement.on('change', this.__show_desired, this);

                    this.dealerBinder = new Backbone.ModelBinder();
                    this.dealerBinder.bind(this.model.dealership, this.$('.dealer-contact'));

                    this.directionsBinder = new Backbone.ModelBinder();
                    this.directionsBinder.bind(this.model.directions, this.$('.directions-section'));

                    this.equipment_container = this.$('[data-section="optional-equipment"]');
                    this.equipment_tpl = _.template(_.unescape(this.equipment_container.html()), null, { variable: 'option' });
                    this.equipment_container.empty();

                    //Set the expiration date of the appraisal
                    var expirationDays = _s.toNumber(this.model.widget_options.get('AppraisalExpiry')) || 0;
                    var expiration = new Date();
                    expiration.setDate(expiration.getDate() + expirationDays);
                    this.$('.expiration-days').html(expirationDays);
                    this.$('.expiration-date').html(_s.sprintf("%02d/%02d/%d", expiration.getMonth() + 1, expiration.getDate(), expiration.getFullYear()))

                },
                __set_tradein_image: function () {
                    this.$('.tradein-panel .vehicle-image img').attr('src', this.model.tradein.get_image());
                },
                __set_desired_image: function () {
                    this.$('.desired-panel .vehicle-image').html(this.model.replacement.get('image'));
                },
                __show_desired: function () {
                    this.$('.desired-panel').addClass('hide');
                    if (!_.isUndefined(this.model.replacement.get('vin'))) {
                        this.$('.desired-panel').removeClass('hide');
                    }
                },
                __draw_options: function () {
                    this.equipment_container.empty().addClass('hide');

                    if (this.collection.options.length > 0) {
                        _.each(this.collection.options.get_selected(true), function (m) {
                            m = m.toJSON();
                            m.value = _s.toNumber(m.value);
                            m.value = isNaN(m.value) ? 0 : m.value;
                            m.value = _s.sprintf("$%s", _s.numberFormat(m.value))
                            var tpl = jQuery(this.equipment_tpl(m));
                            this.equipment_container.append(tpl);
                        }, this)
                        this.equipment_container.removeClass('hide');
                    }
                },
                show_calculator: function (e) {
                    var trigger = jQuery(e.currentTarget);
                    var modal = jQuery(this.modal_template());
                    modal.find('.modal-header h3').html(this.calculator.$el.attr('data-modal-title'))
                    modal.find('.modal-body').append(this.calculator.$el);
                    modal.on('hidden', _.bind(function () {
                        this.modal.remove();
                        this.modal = undefined;
                    }, this));
                    modal.modal({
                        keyboard: true,
                        backdrop: false
                    });
                    this.modal = modal;
                },
                post_lead: function () {
                    var lead = { dealerkey: this.dealerkey };

                    _.each(this.model_to_post_map, function (fields, model) {
                        if (_.isUndefined(this.inst.model[model])) {
                            return;
                        }
                        _.each(fields, function (key, oldkey) {
                            if (_.isNumber(oldkey)) {
                                var key2 = key;
                            }
                            else {
                                var key2 = oldkey;
                            }
                            this.lead[key] = this.inst.model[model].get(key2);
                        }, this)
                    }, { inst: this, lead: lead })

                    lead.phone = lead.phone.join('');

                    if (this.collection.options.length > 0) {
                        var options = this.collection.options.get_selected('name')
                        lead.options = options.join('|');
                    }
                    var images = [];
                    $.each(this.collection.uploads.models, function (index) {
                        images[images.length] = this.attributes.name;
                    });
                    if (images.length > 0) {
                        lead.images = images.join('|');
                    }
                    var videos = this.collection.uploads.get_type('videos', 'name');
                    if (videos.length > 0) {
                        lead.videos = videos.join('|');
                    }

                    if (!_.isUndefined(this.model.replacement.get('vin'))) {
                        lead.selected = this.model.replacement.get('vin')
                    }
                    var baserange = this.model.tradein.get('base_range');
                    var options = this.collection.options.get_selected();
                    _.each(options, function (o, i) {
                        options[i].value = this.format.currency(o.value);
                    }, this)

                    var thisisme = this;

                    if (!thisisme.options.e_params.qmode) {
                        lead.page = this.email_template(_.extend({}, lead, this.model.dealership.toJSON(),
                                                                           this.model.dealership.get('DealerContact'),
                                                                           this.model.widget_options.toJSON(), {
                                                                               thumb_image: this.model.tradein.get_image(),
                                                                               big_image: this.model.tradein.get_big_image(),
                                                                               desired_thumb_image: this.model.replacement.get_image(),
                                                                               desired_big_image: this.model.replacement.get_image(),
                                                                               options: options,
                                                                               base_range: this.format.range(this.model.tradein.get('base_range'), this.model.widget_options.get('EnableValueRanges')),
                                                                               options_adj: this.format.currency(this.model.tradein.get('options_adj')),
                                                                               mileage_adj: this.format.currency(this.model.tradein.get('mileage_adj')),
                                                                               total: this.format.range(this.model.tradein.get('range'), this.model.widget_options.get('EnableValueRanges')),
                                                                               price: this.format.currency(this.model.replacement.get('price')),
                                                                               promo_image: this.model.promotion.get('image'),
                                                                               promo_text: this.model.promotion.get(this.model.default_language.get('language')),
                                                                               expirationdate: this.$('.expiration-date').html()
                                                                           }));

                        lead.page = jQuery("<div>" + lead.page + "</div>");                        
                        if (_.isUndefined(this.model.replacement.get('year'))) {
                            lead.page.find('#PageText_pnlDesiredVehicle').remove();
                        }

                        if (this.$('.promotion-container').find('*').length == 0) {
                            this.$('#PageText_pnlSpecialOffer').remove();
                        }

                        if (this.$('.directions-container').is(':visible')) {
                            lead.page.find('#directions').append(this.$('.directions-container').html());
                        }

                        lead.page = lead.page.html();
                        lead.page = Base64.encode(lead.page);
                    }


                    lead.qmode = false;

                    if (thisisme.options.e_params.qmode) {

                        thisisme.hide_map();

                        lead.qmode = true;
                        lead.qyear = thisisme.options.e_params.year;
                        lead.qmake = thisisme.options.e_params.make;
                        lead.qmodel = thisisme.options.e_params.model;

                        lead.qstocknumber = thisisme.options.e_params.stocknumber;
                        lead.qvehicleid = thisisme.options.e_params.vehicleid;
                        lead.qprice = thisisme.options.e_params.price;

                        lead.qimageurl = thisisme.options.e_params.imageurl;
                        lead.qinternaldealerid = thisisme.options.e_params.internaldealerid;

                        lead.qdname = thisisme.options.e_params.dname;
                        lead.qdaddress = thisisme.options.e_params.daddress;
                        lead.qdcity = thisisme.options.e_params.dcity;
                        lead.qdphone = thisisme.options.e_params.dphone;
                        lead.qdstate = thisisme.options.e_params.dstate;
                        lead.qdzip = thisisme.options.e_params.dzip;

                    }


                    var sLead;
                    if (thisisme.options.e_params.qmode) {
                        sLead = lead;
                    } else {
                        sLead = lead; //JSON.stringify(lead);
                    }

                    
                    jQuery.ajax({
                        url: this.model.appraisal.url,
                        type: 'POST',
                        data: sLead,
                        dataType: "json",
                        //contentType: 'application/json; charset=utf-8',
                        contentType: 'application/x-www-form-urlencoded',
                        error: function (jqXHR, textStatus, errorThrown) {
                            jqXHR = jqXHR;
                            textStatus = textStatus;
                            errorThrown = errorThrown;
                            
                        },
                        success: function (data) {
                            if (!thisisme.options.e_params.qmode) {
                                thisisme.model.dealership.set('AddressForMap', data.cinfoStreet + ', ' + data.cinfoCity + ', ' + data.cinfoState + ', ' + data.cinfoZip);
                                thisisme.show_map();
                            }

                            if (data != null && data.dinfo != null) {
                                thisisme.model.dealership.set('AddressForHtml', data.dinfo.AddressForHtml);
                                thisisme.model.dealership.set('AppointmentEmail', data.dinfo.AppointmentEmail);
                                thisisme.model.dealership.set('DealerName', data.dinfo.DealerName);
                                thisisme.model.dealership.set('City', data.dinfo.City);
                                thisisme.model.dealership.set('State', data.dinfo.State);
                                thisisme.model.dealership.set('Zip', data.dinfo.Zip);
                                thisisme.model.dealership.set('LeadContact', data.dinfo.LeadContact);
                                thisisme.model.dealership.set('DealerContact', data.dinfo.DealerContact);
                                thisisme.model.dealership.set('AssignedKey', data.cinfoKey);
                            }

                            if (data != null) {
                                $('#cinfoName').html(data.cinfoName);
                                $('#cinfoStreet').html(data.cinfoStreet);
                                $('#cinfoCity').html(data.cinfoCity);
                                $('#cinfoState').html(data.cinfoState);
                                $('#cinfoPhone').html(data.cinfoPhone);
                                $('#cinfoEmail').html(data.cinfoEmail);
                            }

                            thisisme.save_apr(data);
                        },
                        context: this
                    })
                },
                save_apr: function (data) {
                    this.model.appraisal.set(data)
                },
                hide_map: function () {
                    this.$('div#map').hide();
                },
                show_map: function () {
                    var mapEl = this.$('div#map').height(400);

                    mapEl.gmap3({
                        marker: {
                            address: this.model.dealership.get('AddressForMap'),
                            tag: 'dealership',
                            title: this.model.dealership.get('AddressForMap')
                        },
                        map: {
                            address: this.model.dealership.get('AddressForMap'),
                            options: {
                                zoom: 13,
                                //center : this.model.dealership.get('AddressForMap'),
                                mapTypeControl: false,
                                navigationControl: false,
                                scrollwheel: false,
                                streetViewControl: false
                            }
                        }
                    });

                    mapEl.gmap3({
                        get: {
                            name: 'marker',
                            tag: 'dealership',
                            callback: _.bind(function (marker) {
                                if (!_.isUndefined(marker["openInfoWindowHtml"])) {
                                    marker.openInfoWindowHtml(this.model.dealership.get('AddressForMap'));
                                }
                                else {
                                    var w = new google.maps.InfoWindow({
                                        content: this.model.dealership.get('AddressForMap')
                                    })
                                    w.open(marker.getMap(), marker);
                                }
                            }, this)
                        }
                    });
                },
                pre_directions: function () {
                    if (this.validate()) {
                        //Clean up any errors
                        this.$('span.error-block').remove();
                        this.get_directions();
                    }
                },
                get_directions: function () {
                    this.$('.directions-container').addClass('hide');
                    this.$('div#map').gmap3({
                        getroute: {
                            options: {
                                origin: this.model.directions.get('from_address'),
                                destination: this.model.dealership.get('AddressForMap'),
                                travelMode: google.maps.DirectionsTravelMode.DRIVING
                            },
                            callback: _.bind(function (results) {
                                if (!results) {
                                    return;
                                }
                                this.$('.directions-container').removeClass('hide');
                                this.$('div#map').gmap3({
                                    map: {
                                        options: {
                                            zoom: 13
                                        }
                                    },
                                    directionsrenderer: {
                                        container: this.$('.directions-pane'),
                                        options: {
                                            directions: results
                                        }
                                    }
                                });
                            }, this)

                        }
                    })
                },
                validate: function () {
                    var validateModels = [this.model.directions];
                    lang = this.model.default_language.get('language');

                    var errors = {};
                    _.each(validateModels, function (model) {
                        _.extend(errors, model.validate_model(lang));
                    }, this)

                    if (_.isEmpty(errors)) {
                        return true;
                    }
                    else {
                        _.bind(show_errors, this)(errors);
                    }
                },
                rename_key: function (object, oldName, newName) {
                    // Check for the old property name to avoid a ReferenceError in strict mode.
                    if (object.hasOwnProperty(oldName)) {
                        object[newName] = object[oldName];
                        delete object[oldName];
                    }
                    return object;
                },
                print: function () {
                    //this.$('#map').hide();
                    this.$el.parents('.widget-app:first').jqprint()
                    //this.$('#map').show();
                },
                print_directions: function () {
                    this.$('.directions-container [data-toggle="print_directions"]').hide();
                    this.$('.directions-container').jqprint()
                    this.$('.directions-container [data-toggle="print_directions"]').show();
                }
            }
        });