define(['jquery',
        'backbone',
        'underscore',
        'underscore.string',
        'text!tpl/promotion.html'], function (jQuery, Backbone, _, _s, PromoTPL) {
            return {
                template: _.template(PromoTPL, null, { variable: 'promo' }),
                tag: 'div',
                initialize: function (options) {
                    this.dealerkey = this.options.dealerkey;
                    this.containers = new Backbone.Collection();    
                    //debugger;

                    if (this.model.get('enabled') === true) {
                        this.model.set('text', this.model.attributes.ENG);                        
                        this.model.set('image', this.options.URL('promotion_image') + '&dealerkey=' + this.dealerkey);                        
                        this.$el.append(this.template(this.model.toJSON()));
                        if (this.model.get('hasimage') !== true) {
                            this.$('.thumbnail').hide();
                        }
                    }
                    this.model.on('change', this.render, this);
                    this.containers.on('change add', this.append, this);
                },
                render: function () {

                    this.enabled = this.model.get('enabled') == true;
                
                    if (this.model.get('hasimage') !== true) {
                       this.$('.thumbnail').hide();
                    }
                    if (this.enabled) {
                        this.$el.show();
                    }
                    else {
                        this.$el.hide();
                    }
                    this.$('p').html(this.model.get(this.model.get('language')));
                    this.containers.trigger('change');
                    return this;
                },
                append_to: function (el) {
                    this.containers.add({ el: el });
                },
                append: function () {
                    this.containers.forEach(function (el) {
                        el = el.get('el');
                        jQuery(el).empty().append(jQuery(this.$el).clone())
                    }, this)
                }
            }
        });