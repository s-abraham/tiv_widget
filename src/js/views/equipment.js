/*
 * @author Freddy Knuth
 * 
 * Appraisal View - This handles the process of letting the consumer 
 * find the vehicle they want to trade in
 * 
 */

define(['jquery', 
        'underscore', 
        'underscore.string', 
        'backbone', 
        'bootstrap', 
        'jquery.uniform', 
        'jquery.selectbox'], 
        function(jQuery, _, _s, Backbone) {
    return {
        show_price: true,
        events : {
            'change :input' : 'save_options'
        },
        initialize : function() {
            this.collection.on('change', this.sum_options, this);
            this.collection.on('reset', this.draw, this);

            this.set_defaults();
        },
        set_defaults : function() {
            //Container for the total sum of the options
            this.options_total = this.$('.options-total');
            
            //Container for the optional equipment
            this.equip_container = this.$('.panel-content');
            
            //templates for optional equipment
            this.option_template = this.equip_container.find('ul').html();
            this.option_template = _.unescape(this.option_template);
            this.option_template = _.template(this.option_template, null, {
                variable : 'option'
            });
            //Empty the container
            this.equip_container.find('ul').empty();
        },
        show : function() {
            this.$el.slideDown();
        },
        hide : function() {
            this.$el.slideUp();
        },
        //Resets the dropdowns 
        clean : function(e) {
            //Clear options and the UI
            this.collection.reset([]);
            if(this.$el.is(':visible')) {
                this.$el.slideUp(100, _.bind(function() {
                    this.$el.find('ul').empty();
                }, this));
            }
            else {
                this.$el.find('ul').empty();
            }
        },
        draw : function() {
            if(this.collection.length < 1) {
                return;
            }
            this.collection.forEach(function(option, i) {
                var optionEl = jQuery(this.option_template({
                    text: option.get('name'),
                    value: _s.numberFormat(parseInt(option.get('value')), 2) 
                }));
                optionEl.data('model', option);
                this.$el.find('ul').append(optionEl);
                //Fixes a very specific bug in older Android browsers where the name is never rendered DM - 03.13.2013
                optionEl.find('span.option-text').html(option.get('name'));
                option.on('change:selected', this.display_price, {model: option, el: optionEl, show_price: this.show_price});
            }, this);
            this.options_total.html('$' + _s.numberFormat(0, 2))
            this.show();
        },
        display_price: function() {
            if(this.model.get('selected') === true && this.show_price === true)
            {
                this.el.find('.option-value').removeClass('hide');
            }
            else if(this.el.find('.option-value').hasClass('hide') === false)
            {
                this.el.find('.option-value').addClass('hide');
            }
        },
        save_options : function(e) {
            var trigger = jQuery(e.currentTarget);
            var model = trigger.parents('li:first').data('model');
            if(trigger.is(':checked'))
            {
                model.set('selected', true);    
            }
            else
            {
                model.set('selected', false);
            }
        },
        sum_options : function() {
            var sum = this.collection.get_total();
            this.options_total.html('$' + _s.numberFormat(sum, 2))
        }
    }
}); 