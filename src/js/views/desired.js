
define(['jquery',
        'underscore',
        'underscore.string',
        'backbone',
        'bootstrap'], function (jQuery, _, _s, Backbone) {
            return {
                start_loader: 'category',
                dropdowns: {},
                car_values: ['category', 'make', 'model'],
                events: {
                    'change [name="condition"]': 'set_isnew',
                    'change [data-trigger=load]': 'clean_options',
                    'change [data-loader]': 'get_values',
                    //'click [data-toggle="reset"]': 'reset',
                    //'click [data-toggle="next"]': 'next'
                },
                rendered: false,
                initialize: function () {
                    this.dealerkey = this.options.dealerkey;
                    this.e_params = this.options.e_params;
                    
                    this.language = 'english';

                    this.set_defaults();
                },
                set_isnew: function (e) {
                    var val = jQuery(e.currentTarget).val();

                    _.each(this.car_values, function (k) {
                        this.model.unset(k, { silent: false })
                    }, this)

                    if (val == 'unsure') {
                        this.$('.desired-field').hide();
                        this.model.set('neworused', null);
                    }
                    else {
                        
                        this.model.set('neworused', _s.capitalize(val));
                        this.$('.desired-field').show();
                        this.get_values({
                            currentTarget: e.currentTarget
                        });

                        setTimeout(function(){ 
                                $('#category').change();
                                this.$('.desired-field').show();
                        }, 20);
                    }

                },
                set_defaults: function () {
                    //Override URLSs in case it's needed
                    if (_.isObject(this.options.URL)) {
                        _.extend(this.URL, this.options.URL)
                    }

                    //Set dealerkey to URLs in case it's needed, and other default
                    // args
                    _.each(this.URL, function (v, k) {
                        if (v.indexOf('dealerkey') >= 0) {
                            this.URL[k] = _s.sprintf(v, this.dealerkey)
                        }
                    }, this);

                    //Create bare templates of each SELECT to be used for resetting
                    // them
                    _.each(this.$('select[data-loader]'), function (e) {
                        this.dropdowns[e.name] = _.template(jQuery(e).parent().html(), null, {
                            variable: 'select'
                        });
                    }, this)
                },
                render: function () {
                    if (!this.rendered) {
                        //this.$(':input[type="radio"]').uniform();
                        this.$('select').selectBox();
                        this.rendered = true;
                    }
                },
                clean_options: function (e) {
                    var trigger = jQuery(e.currentTarget);
                    var type = trigger.attr('name');
                    var index = this.car_values.indexOf(type) + 1;
                    // if(index == 0) {
                    // index++;
                    // }
                    var list = this.car_values.slice(index);

                    //Clean up any errors on the one triggering the action
                    this.$('select[data-trigger=load][name=' + type + ']').parent().find('span.error-block').remove();


                    _.each(list, function (t) {
                        if (t == 'condition') {
                            return;
                        }
                        var e = this.dropdowns[t]
                        var o = this.$('select[data-trigger=load][name=' + t + ']')
                        if (e && o) {
                            var p = o.parent().html(e);
                            p.find('select').selectBox();
                            this.model.unset(t);
                        }
                    }, this);
                },
                get_values: function (e) {
                    var type = this.start_loader;
                    if (!_.isUndefined(e) && !_.isUndefined(e.currentTarget)) {
                        var trigger = jQuery(e.currentTarget);
                        var type = trigger.attr('data-loader');
                        this.model.set(trigger.attr('name'), trigger.val());
                    }

                    if (!_.isNull(this.model.get('model')) &&
                       !_.isUndefined(this.model.get('model'))) {
                        return;
                    }
                    
                    if (!this.model.attributes.e_params.qmode) {
                        var data = this.model.toJSON();

                        jQuery.ajax({
                            //accepts : 'application/json',
                            url: this.model.url,
                            data: data,
                            context: {
                                inst: this,
                                type: type,
                                condition: data.condition
                            },
                            success: this.fill_values
                        });
                    }
                },
                fill_values: function (data, jqXHR) {
                    var key;
                    if (_.isObject(data)) {
                        if (!_.isUndefined(data[this.type + 's'])) {
                            key = this.type + 's';
                        }
                        else if (this.type == 'category') {
                            key = 'categories';
                        }
                        else {
                            key = this.type;
                        }
                        this.inst.fill_dropdown(data[key], this.type);
                    }
                },
                fill_dropdown: function (data, type) {
                    var fill_trigger = this.dropdowns[type];
                    var o = this.$('select[data-trigger=load][name=' + type + ']')
                    var p = o.parent();
                    o.selectBox('destroy');
                    p.empty();
                    p.append(fill_trigger);
                    fill_trigger = p.find('select');
                    _.each(data, function (value, i) {
                        var option = document.createElement('option');
                        option.text = option.value = value;
						option.innerText = value;
                        if(value === "All"){   option.setAttribute('data-lang', 'customer.desired.all'); }
                        this.append(option);
                    }, fill_trigger)

                    fill_trigger.selectBox();
                    fill_trigger.selectBox('refresh');
                }
            }
        });