define(['underscore', 
        'underscore.string',
        'jquery', 
        'backbone.modelbinder'], function(_, _s, jQuery, CalcTPL) {
    
    if(!_.isUndefined(jQuery.fn.autotab))
    {
        jQuery.fn.autotab = function(at_next, spacetab) {
            jQuery(this).data('at_next', at_next).addClass('autotab').attr('spacetab', spacetab);
            jQuery(at_next).data('at_prev', this);
            return this;
        };    
    }
    
    return {
        events : {
            'keyup .autotab' : 'autotab',
            'keydown .numeric' : 'numeric',
            'click :input': 'highlight',
            //'focus :input': 'highlight',
            'click button': 'total'
        },
        converter: {
            range: function(direction, value) {
                if(direction == "ModelToView" && _.isArray(value))
                {
                    var value1 = _s.toNumber(value[0]);
                    value1 = isNaN(value1) ? 0 : value1;
                    var value2 = _s.toNumber(value[1]);
                    value2 = isNaN(value2) ? 0 : value2;
                    
                    return _s.sprintf("$%s to $%s", _s.numberFormat(value1, 0), _s.numberFormat(value2, 0));
                }
                return value;
            },
            currency: function(direction, value) {
                if(direction == "ModelToView")
                {
                    var number = _s.toNumber(value);
                    number = isNaN(number) ? 0 : number;
                    var format = "$%s";
                    if(number < 0)
                    {
                        number *= -1;
                        format = "($%s)"
                    }
                    
                    return _s.sprintf(format, _s.numberFormat(number, 0));
                }
                return value;
            },
            number: function(direction, value) {
                if(direction == "ModelToView")
                {
                    return _s.numberFormat(_s.toNumber(value), 0);
                }
                return value;
            }
        },
        initialize : function() {
            var bindings = Backbone.ModelBinder.createDefaultBindings(this.el, 'name');
            bindings.price.converter = bindings.down_payment.converter = this.converter.number;
            bindings.total.converter = bindings.trade_in_value.converter = this.converter.number;
            
            bindings.interest_rate.converter = bindings.loan_length.converter = this.converter.number;
                        
            var calcBinder = new Backbone.ModelBinder();
            calcBinder.bind(this.model, this.$el, bindings);
        },
        total: function() {
            this.model.total();
        },
        blur: function(e) {
            
        },
        highlight: function(e) {
            jQuery(e.currentTarget).val(jQuery(e.currentTarget).val()).select();
            
        },
        render: function() {
            return this;
        },
        numeric : function(event) {
            return (event.which >= 48 && event.which <= 57) ||
                   (event.which >= 96 && event.which <= 105) || 
                   (event.which == 8 || event.which == 46 || event.which == 9);
        },
        autotab : function(event) {
            var code = event.which;
            var el = event.currentTarget;
            var maxlength = jQuery(el).attr('maxlength');
            var spaceOnTab = jQuery(el).attr('spacetab');
            var value = jQuery(el).val();
            if(value.length == maxlength && (code >= 32)) 
            {
                var at_next = jQuery(el).data('at_next');
                jQuery(at_next).focus().select();
            }
            else if(value.length == 0 && code == 8) 
            {
                jQuery(el).val('');
                var at_prev = jQuery(el).data('at_prev');
                jQuery(at_prev).focus().select();
            }
        }
    }
}); 