define(['jquery',
        'underscore',
        'underscore.string',
        'jquery-ui-datepicker',
        'backbone.modelbinder'], function (jQuery, _, _s) {


            var show_errors = function (errors) {
                this.$('.error-block').remove();
                _.each(errors, function (message, k) {
                    var error = jQuery(document.createElement('span'));
                    error.addClass('error-block');
                    error.html("*" + message);

                    var input = this.$(':input[name=' + k + '],.input-' + k);

                    if (input[0].type == "radio") {
                        var row = input.parents('.condition-group:first');
                        row.append(error);
                    }
                    else {
                        var row = input.parents(':first');
                        row.append(error);
                    }

                }, this);
            }


            return {
                events: {
                    'click button[name="addappt"]': 'submit_appt'
                },
                initialize: function () {
                    this.dealerkey = this.
                    $(":input[name=apptdate]").datepicker({
                        showOn: "both",
                        buttonImage: "img/calender-black.jpg",
                        buttonImageOnly: true,
                        minDate: new Date()
                    });
                    this.customerBinder = new Backbone.ModelBinder();
                    this.customerBinder.bind(this.model.appointment, this.$el);

                    this.language_tpl = this.options.language_tpl

                },
                submit_appt: function () {
                    if (this.validate()) {
                        //Clean up any errors
                        this.$('span.error-block').remove();
                        

                        var innerData = {};

                        innerData.apptemail = this.language_tpl[this.model.default_language.get('language')];

                        innerData.apptsubject = 'Appointment Request For ';
                        innerData.apptdate = this.model.appointment.attributes.apptdate;
                        innerData.appttime = this.model.appointment.attributes.appttime;
                        innerData.aprkey = this.model.appointment.attributes.aprkey;
                        innerData.assignedkey = this.model.dealership.attributes.AssignedKey;
                        innerData.dealerkey = this.model.appointment.attributes.dealerkey;

                        innerData.email = this.model.customer.attributes.email;
                        innerData.firstname = this.model.customer.attributes.firstname;
                        innerData.lastname = this.model.customer.attributes.lastname;
                        innerData.phone = this.model.customer.attributes.phone.join('-');
                        innerData.zipcode = this.model.customer.attributes.zip;

                        var postData = { model: JSON.stringify(innerData) }


                        jQuery.ajax({
                            type: "POST",
                            url: this.model.appointment.url,                            
                            data: postData,                            
                            success: this.success,
                            dataType: "json",
                            context: this
                        });
                    }
                },
                success: function (data) {
                    this.$('.appointment-success').html('Appointment Created').show();
                    this.$('button').attr('disabled', 'disabled');
                },
                validate: function () {
                    var validateModels = [this.model.appointment];
                    lang = this.model.default_language.get('language');

                    var errors = {};
                    _.each(validateModels, function (model) {
                        _.extend(errors, model.validate_model(lang));
                    }, this)

                    if (_.isEmpty(errors)) {
                        return true;
                    }
                    else {
                        _.bind(show_errors, this)(errors);
                    }
                },
            }
        });