define(['jquery', 
        'underscore', 
        'underscore.string', 
        'backbone', 
        'text!tpl/modal.html',
        'text!tpl/modal.car.html',
        'bootstrap', 
        'jquery.uniform', 
        'jquery.selectbox',
        'jquery.colorbox'], function(jQuery, _, _s, Backbone, Modal, CarModal) {
    return {
        modal_template: _.template(Modal, null, {variable: 'modal'}),
        modal_car_template: _.template(CarModal, null, {variable: 'item'}),
        events : {
            'click .pager a': 'paginate',
            'click [data-trigger="car_popup"]': 'show_details',
            'change :input[type="checkbox"][name="replacement_vehicle"]': 'set_replacement_vehicle'
        },
        initialize : function() {
            this.dealerkey = this.options.dealerkey;
            this.language = 'english';
            
            this.set_defaults();
            this.limit = new this.model.pager({
                limit: 2,
                offset: 0
            })
            
            this.results_container = this.$('.inventory-results');
            this.item_template = _.template(_.unescape(this.results_container.html()), null, {variable: 'item'})
            this.results_container.empty();
            
            this.model.inventory.on('change', this.process_inventory, this);
            this.model.desired.on('change', this.get_inventory, this);
            this.limit.on('change:offset change:limit', this.get_inventory, this);
            this.collection.on('reset', this.draw_inventory, this);
            
        },
        set_defaults : function() {
            //Override URLSs in case it's needed
            if(_.isObject(this.options.URL)) {
                _.extend(this.URL, this.options.URL)
            }

            //Set dealerkey to URLs in case it's needed, and other default
            // args
            _.each(this.URL, function(v, k) {
                if(v.indexOf('dealerkey') >= 0) {
                    this.URL[k] = _s.sprintf(v, this.dealerkey)
                }
            }, this);
        },
        set_replacement_vehicle : function(e) {
            var trigger = e.currentTarget;

            _.each(this.results_container.find(':input'), function(el) {
                if(el != trigger)
                {
                    el.checked = false;    
                }
            }, trigger);

            var model = jQuery(trigger).parents('.inventory-item:first').data('car');
            this.model.replacement.set(model.toJSON());
        },
        paginate: function(e) {
            var trigger = jQuery(e.currentTarget);
            var direction = trigger.attr('data-trigger');
            e.preventDefault();
            this.limit[direction]();
        },
        get_inventory : function(e) {
            if(_.isUndefined(this.model.desired.get('make')) || _.isUndefined(this.model.desired.get('model')))
            {
                return;
            }
            
            var data = _.extend({}, this.model.desired.toJSON(), _.omit(this.limit.toJSON(), 'max'));
            this.model.inventory.fetch({
                data: data
            })
        },
        process_inventory: function() {
            this.limit.set('max', this.model.inventory.get('totalrows'))
        },
        draw_inventory: function() {
            this.results_container.empty();
            if(this.collection.length  > 0)
            {
                this.collection.forEach(function(car) {
                    var carO = car.toJSON();
                    carO.price = _s.numberFormat(carO.price, 0);
                    var tpl = jQuery(this.item_template(carO));
                    tpl.data('car', car);
                    this.results_container.append(tpl)                    
                }, this)
                this.$el.show();
            }
            else
            {
                this.$el.hide();
            }
            
        },
        show_details: function(e) {
            var trigger = jQuery(e.currentTarget);
            var car = trigger.parents('.inventory-item').data('car'); 
            var modal = jQuery(this.modal_template());
            var car_modal = jQuery(this.modal_car_template(car.toJSON()));
            
            var option_tpl = car_modal.find('#option-template').html();

            e.preventDefault();

            _.each(car.get('options'), function(opt) {
                var tpl = jQuery(this.tpl).clone();
                tpl.find('td').html(opt);
                this.main.find('.vehicle-options tbody').append(tpl);
            }, {tpl: option_tpl, main: car_modal})
            modal.find('.modal-header h3').html(car_modal.attr('data-modal-title'))
            modal.find('.modal-body').append(car_modal);
            modal.on('hidden', _.bind(function() 
            {
                this.modal.remove();
                this.modal = undefined;
            }, this));
            modal.modal({
                keyboard: true,
                backdrop: false
            });
            this.modal = modal;
        }
    }
}); 