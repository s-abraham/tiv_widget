define(['jquery'], function(jQuery) {
    jQuery.fn.autotab = function(at_next, spacetab) {
        jQuery(this).data('at_next', at_next).addClass('autotab').attr('spacetab', spacetab);
        jQuery(at_next).data('at_prev', this);
        return this;
    };
    
    return {
        events : {
            'keyup .autotab' : 'autotab',
            'keydown .numeric' : 'numeric'
        },
        initialize : function() {
            this.dealerkey = this.options.dealerkey;
            
            var phoneFields = this.$('[name*=phone]').addClass('numeric');
            jQuery(phoneFields[0]).autotab(phoneFields[1]);
            jQuery(phoneFields[1]).autotab(phoneFields[2]);
        },
        numeric : function(event) {
            return (event.which >= 48 && event.which <= 57) ||
                   (event.which >= 96 && event.which <= 105) || 
                   (event.which == 8 || event.which == 46 || event.which == 9);
        },
        autotab : function(event) {
            var code = event.which;
            var el = event.currentTarget;
            var maxlength = jQuery(el).attr('maxlength');
            var spaceOnTab = jQuery(el).attr('spacetab');
            var value = jQuery(el).val();
            if(value.length == maxlength && (code >= 32)) 
            {
                var at_next = jQuery(el).data('at_next');
                jQuery(at_next).focus().select();
            }
            else if(value.length == 0 && code == 8) 
            {
                jQuery(el).val('');
                var at_prev = jQuery(el).data('at_prev');
                jQuery(at_prev).focus().select();
            }
        }            
    }
}); 