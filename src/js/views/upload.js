define(['underscore', 
        'jquery', 
        'jquery.fileupload'], function(_, jQuery) {
    return {
        progress: false,
        events : {
            'fileuploadadd': 'added',
            'fileuploaddone': 'done',
            'click button.tiv-btn-photo-modal': 'close',
            'click button.tiv-btn-video-modal.upload': 'close',
            'click button.tiv-btn-video-modal.url': 'save'
        },
        initialize : function() {
            this.$('form').attr('action', this.collection.url);
            this.upload_container = this.$('.media-container');
            this.upload_template = this.upload_container.html();
            this.upload_template = _.template(this.upload_template, null, {variable: 'upload'});
            this.initialize_fileupload();
        }, 
        initialize_fileupload: function() {
            var el = !_.isUndefined(arguments[0]) ? jQuery(arguments[0]) : this.upload_container.find('.image-row:first'); 
            this.fu = el.fileupload({
                dataType: 'json',
                autoUpload: true,
                formData: {
                    dealerkey: this.options.dealerkey
                },
                progressall: _.bind(this.display_progress, {el: el, inst: this})
            });
        },
        added: function(e, data) {
            var trigger = jQuery(e.target);
            var row = trigger.parents('.image-row:first').andSelf();
            row.find('span').attr('disabled', 'disabled');
            row.find(':input').attr('disabled', 'disabled');            
            if(this.options.key == 'images')
            {
                var tpl = jQuery(this.upload_template());
                this.upload_container.append(tpl);
                this.initialize_fileupload(tpl);    
            }
        },
        done: function(e, data) {
            var trigger = jQuery(e.target);
            trigger.fileupload('disable');
            if(_.isObject(data))
            {
                data = data.result;
            }
            this.$('#progress .bar').css('width', '100%');
            _.each(data, function (file) {                
                var d = _.extend({}, file, {media_type: this.options.key})
                this.collection.add(d)
                trigger.find('#progress .bar').html(d.name)
            }, this);
            this.progress = false;
        },
        display_progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            this.el.find('#progress .bar').css(
                'width',
                progress + '%'
            ).html(progress + '%');
            this.inst.progress = true;
        },
        clean: function() {
            this.$('.media-container').empty().append(this.upload_template());
            this.collection.reset([])
            this.progress = false;
            this.close();
        },
        save: function() {
            this.collection.add({type: 'videos', name: this.$(':input[name="video"]').val()})
            this.close();
        },
        close: function() {
            var hiding = _.isBoolean(arguments[0]) ? arguments[0] : false;
            if(this.progress)
            {
                this.$('.error').html('There are uploads still in progress');
                return false;
            }
            else
            {
                this.trigger('upload_done', hiding)
                return true;
            }
            
        }        
    }
}); 