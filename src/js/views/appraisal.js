/*
 * @author Freddy Knuth
 * 
 * Appraisal View - This handles the process of letting the consumer 
 * find the vehicle they want to trade in
 * 
 */

define(['jquery',
        'underscore',
        'underscore.string',
        'text!tpl/modal.html',
        'bootstrap',
        'jquery.uniform',
        'jquery.selectbox'],
        function (jQuery, _, _s, Modal) {
            return {
                condition: true,
                errors: [],
                start_loader: 'year',
                dropdowns: {},
                car_values: ['year', 'make', 'model', 'trim'],
                modal_template: _.template(Modal, null, { variable: 'modal' }),
                events: {
                    'change [data-trigger=load]': 'clean',
                    'change [data-loader]': 'get_values',
                    'click [data-trigger=upload]': 'upload_media',
                    'click #pop_condition a': 'show_condition_modal'
                },
                initialize: function () {
                    this.dealerkey = this.options.dealerkey;                    
                    this.condition = this.options.condition;
                    this.condition2 = this.options.condition2;

                    this.set_defaults();
                    this.get_values();
                },
                upload: {
                    image: null,
                    video: null
                },
                skin_input: function (el) {
                    if (_.isUndefined(el)) {
                        this.$(':input[type="radio"],:input[type="checkbox"],:input[type="text"]').uniform();
                        //this.$(':input[type="checkbox"],:input[type="text"],:input[type="tel"],:input[type="number"]').uniform();
                        this.$('select').selectBox();
                    }
                    else {
                        if (jQuery(el).find('select').length > 0) {
                            jQuery(el).find('select').selectBox();
                        }
                        else {
                            jQuery(el).selectBox();
                        }

                    }
                },
                destroy_input: function (el) {
                    jQuery(el).selectBox('destroy');
                },
                show_condition_modal: function(e){
                    e.preventDefault();

                    if(!jQuery.browser.mobile){
                        
                        var el = this.$('#tradein-condition');
                        var label = jQuery(el).find('h3').html();
                        var text = _s.trim(el.html());
                        
                        if (!_.isUndefined(this.modal)) {
                            this.modal.remove();
                            this.modal = undefined;
                        }else{
                            var modal = jQuery(this.modal_template());
                            modal.find('.modal-header h3').html(label)
                            modal.find('.modal-body').append(text);
                            modal.on('hide', _.bind(function () {
                                return this.close(true);
                            }))
                            modal.on('hidden', _.bind(function () {
                                this.modal.remove();
                                this.modal = undefined;
                            }, this));
                            modal.modal({
                                keyboard: true,
                                backdrop: false
                            });
                            this.modal = modal;
                        }
                    }


                },
                set_defaults: function () {
                    //This is the gap where the video is supposed to be on top
                    this.first_gap = this.$('.first-gap');
                    //This is the form for the trade in values
                    this.form = this.$('.tiv-form');
                    //Container that shows the found trade in
                    this.desc = this.$('.appraisal-desc');

                    //Create bare templates of each SELECT to be used for resetting them
                    _.each(this.$('select[data-loader]'), function (e) {
                        this.dropdowns[e.name] = _.template(jQuery(e).parent().html(), null, {
                            variable: 'select'
                        });
                    }, this)

                        
                    /*

                    if(!jQuery.browser.mobile){
                    //Create popovers for conditions
                    var self = this;
                        _.each(this.$('.condition-group :input'), function (el) {

                            jQuery(el).change(function () {                                                        

                                if (_.isUndefined(self.first_gap)) {
                                    self.fill_car_description();
                                }
                            });

                            var t = el.id.split('-')[1];
                            var text = _s.trim(this.$('#text-tradein-' + t).html());
                            var label = jQuery(el).parents('label:first');
                            label.popover({
                                animation: false,
                                title: _s.capitalize(t),
                                content: text,
                                trigger: 'click',
                                delay: { hide: 10 },
                                placement: function (context, source) {
                                    jQuery(context).addClass('condition-popover').css('visibility', 'hidden').css('position', 'absolute').css('top', 0).appendTo(document.body);

                                    var position = jQuery(source).position();
                                    var popHeight = jQuery(context).height();
                                    var viewHeight = jQuery(window).height();
                                    var scrollTop = jQuery(window).scrollTop();

                                    jQuery(context).detach().css('visibility', '').css('position', '').css('top', '');

                                    if (viewHeight - popHeight - scrollTop - position.top < 0) {
                                        return "top";
                                    }
                                    return "bottom";
                                }

                            });


                        }, this);
                    }

                    */

                },
                //Resets the dropdowns 
                clean: function (e) {

                    //Figure out which element is clearing out options and start from there
                    var trigger = jQuery(e.currentTarget);
                    var type = trigger.attr('name');
                    //Index of the element that initiated the cleaning
                    var index = this.car_values.indexOf(type) + 1;


                    //delete this.desc;
                    //this.desc = this.$('.appraisal-desc');

                    //If its the first, start at the next one
                    if (index === 0) {
                        index++;
                    }
                    //Go through the elements past index and reset them 
                    var keep = this.car_values.slice(0, index).concat(['dealerkey', 'condition', 'mileage']);
                    var list = this.car_values.slice(index);

                    //Clean up any errors on the one triggering the action
                    this.$('select[data-trigger=load][name=' + type + ']').parent().find('span.error-block').remove();

                    _.each(list, function (t) {
                        this.model.unset(t);
                        if (t === 'year') {
                            return;
                        }
                        var e = this.dropdowns[t];
                        var o = this.$('select[data-trigger=load][name=' + t + ']');
                        var p = o.parent().html(e);
                        this.skin_input(p);
                    }, this);
                    
                    if (this.desc.is(':visible')) {
                        this.desc.hide();
                    }

                    this.trigger('clean');
                },
                //Get values for dropdowns
                get_values: function (e) {
                    var type = this.start_loader;
                    
                    if (!_.isUndefined(e) && !_.isUndefined(e.currentTarget)) {
                        var trigger = jQuery(e.currentTarget);
                        var type = trigger.attr('data-loader');
                        this.model.set(trigger.attr('name'), trigger.val());
                    }
                    var data = this.model.toJSON();

                    //debugger

                    delete data.image;
                    delete data.images;
                    delete data.title;
                    delete data.options;

                    //Forces model to be updated every time
                    this.model.unset('image');
                    this.model.unset('images');
                    this.model.unset('title');
                    this.model.unset('options');
                   
                    jQuery.ajax({
                        url: this.model.url,
                        data: data,
                        context: {
                            inst: this,
                            type: type
                        },
                        success: this.fill_values
                    })
                },
                //Fill the values of dropdowns
                fill_values: function (data, jqXHR) {

                    if (_.isObject(data)) {
                        if (!_.isUndefined(data[this.type + 's'])) {
                            key = this.type + 's';
                        }
                        else {
                            key = this.type;
                        }
                        
                        
                        if (key != 'options') {
                            this.inst.fill_dropdown(data[key], this.type);
                        }
                        else {
                            this.inst.model.set(data);
                            this.inst.fill_car_description();
                            this.inst.collection.reset(data[key]);
                        }
                    }
                },
                fill_car_description: function (images) {
                    if (!_.isUndefined(this.first_gap)) {
                        this.first_gap.insertAfter(this.first_gap.siblings('.span5:first'));
                        this.first_gap.removeClass('span2').addClass('span1')
                        // this.first_gap.remove();
                        this.first_gap = undefined;
                    }

                    this.desc.find('.car-title').html();
                    //this.desc.find('.car-trim').html(this.model.get('trim') + '<br />' + this.model.get('condition') + ' condition');
                    this.desc.find('.car-trim').html(this.model.get('trim'));
                    this.desc.find('img').attr('src', this.model.get_image());

                    if (!this.desc.is(':visible')) {
                        this.desc.show();
                    }

                },
                fill_dropdown: function (data, type) {
                    var fill_trigger = this.dropdowns[type];
                    var o = this.$('select[data-trigger=load][name=' + type + ']')
                    var p = o.parent();
                    this.destroy_input(o);
                    p.empty();
                    p.append(fill_trigger);
                    fill_trigger = p.find('select');
                    _.each(data, function (value, i) {
                        if (i == 0) {
                            return;
                        }
                        var option = document.createElement('option');
                        option.text = option.value = value;
						option.innerText = value;
                        this.append(option);
                    }, fill_trigger)
                    //After select has been built, skin it
                    this.skin_input(fill_trigger);
                },
                upload_media: function (e) {
                    var trigger = jQuery(e.currentTarget);
                    var type = trigger.attr('data-upload');
                    var view = this.upload[type];
                    if (_.isUndefined(view)) {
                        return;
                    }
                    if (!_.isUndefined(this.modal)) {
                        this.modal.remove();
                        this.modal = undefined;
                    }

                    var modal = jQuery(this.modal_template());
                    modal.find('.modal-header h3').html(view.$el.attr('data-modal-title'))
                    modal.find('.modal-body').append(view.$el);
                    modal.on('hide', _.bind(function () {
                        return this.close(true);
                    }, view))
                    view.on('upload_done', _.bind(function (hide) {
                        if (hide !== true) {
                            this.modal('hide')
                        }

                    }, modal))
                    modal.on('hidden', _.bind(function () {
                        this.modal.remove();
                        this.modal = undefined;
                    }, this));
                    modal.modal({
                        keyboard: true,
                        backdrop: false
                    });
                    this.modal = modal;
                }
            }
        });