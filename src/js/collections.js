define(['backbone', 'models'], function(Backbone, Model) {
    var collections = {
        options: Backbone.Collection.extend({
            model: Model.equipment_option,
            get_selected: function() {
                var options = [];
                var models = _.isBoolean(arguments[0]) ? arguments[0] : false;
                var key = _.isString(arguments[0]) ? arguments[0] : false;
                _.each(this.where({
                    selected : true
                }), function(m) {
                    if(key !== false) {
                        this.push(m.get(key))
                    }
                    else if(models !== false) {
                        this.push(m);
                    }
                    else {
                        this.push(m.toJSON())    
                    }
                }, options)
                return options;
            },
            get_total: function() {
                return _.reduce(this.get_selected('value'), function(t, v) { 
                    return parseInt(t) + parseInt(v) 
                }, 0)
            }
        }),
        inventory: Backbone.Collection.extend({
            model: Model.inventory_car
        }),
        directions: Backbone.Collection.extend({
                        
        }),
        uploads: Backbone.Collection.extend({
            model: Model.upload,
            get_type: function(type) {
                var options = [];
                var models = _.isBoolean(arguments[1]) ? arguments[0] : false;
                var key = _.isString(arguments[1]) ? arguments[0] : false;
                _.each(this.where({
                    type : type
                }), function(m) {
                    if(key !== false) {
                        this.push(m.get(key))
                    }
                    else if(models !== false) {
                        this.push(m);
                    }
                    else {
                        this.push(m.toJSON())    
                    }
                }, options)
                return options;
            }
        })
    };
    return collections;
});